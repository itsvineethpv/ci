#!/usr/bin/python
# Name: master_script.py
# Author: Swarit Agarwal, swarit.agarwal@philips.com
# Description: This Script is entry point for onboard, wipeout and migrate tenant script
# Created: 17th November 2016
# Modified: 17th November 2016
# #
# Copyright Philips India, 2016
#


import argparse
import os
import errno
import yaml
from ds_functions.migrate_tenant import migrate_tenant
from ds_functions.onboard_tenant import onboard_tenant
from ds_functions.wipe_out_tenant import wipe_out_tenant

from ds_functions.create_fhir_org import create_fhir_org
from lib import tenant_settings
from lib.common_calls import configure_logging
from lib.common_calls import get_log_name


def load_foundation_config(foundation_config_file):
    if foundation_config_file is not None:
        with open(foundation_config_file, 'r') as foundation_cfg_file:
            foundation_keys = yaml.load(foundation_cfg_file)

    tenant_settings.idm_hmac_id = foundation_keys['shared_key']
    tenant_settings.idm_hmac_secret = foundation_keys['secret_key']
    tenant_settings.iam_host = foundation_keys['iam_host']
    tenant_settings.idm_host = foundation_keys['idm_host']
    tenant_settings.idm_enterprise_user_login = foundation_keys['username']
    tenant_settings.idm_enterprise_user_password = foundation_keys['password']
    tenant_settings.oauth_client_id = foundation_keys['client_id']
    tenant_settings.oauth_client_secret = foundation_keys['client_secret']
    # get the forgerock server configs
    tenant_settings.forgerock_host = foundation_keys['openidm_host']
    tenant_settings.forgerock_adminuser = foundation_keys['openidm_user']
    tenant_settings.forgerock_adminpass = foundation_keys['openidm_password']


def load_config(config_file_path):
    with open(config_file_path, 'r') as cfgfile:
        config = yaml.load(cfgfile)
    for section in config:
        config_json = config[section]
        if section == 'cdr.server':
            tenant_settings.fhir_org_identifier_system = config_json[tenant_settings.KEY_CDR_ORG_IDENTIFIER_SYSTEM]
            tenant_settings.full_qualified_org_name = config_json[tenant_settings.KEY_CDR_FULL_ORG_NAME]
            ehr_json = config_json['ehr']
            tenant_settings.cdr_fhir_host_ehr = ehr_json[tenant_settings.KEY_HOST]
            tenant_settings.ehr_hcadmin_user_id = ehr_json[tenant_settings.KEY_CDR_HCADMIN_USER_ID]
            tenant_settings.ehr_hcadmin_user_password = ehr_json[tenant_settings.KEY_CDR_HCADMIN_USER_PASSWORD]
            phr_json = config_json['phr']
            tenant_settings.cdr_fhir_host_phr = phr_json[tenant_settings.KEY_HOST]
            tenant_settings.phr_admin_user_id = phr_json[tenant_settings.KEY_CDR_PHR_ADMIN_USER_ID]
            tenant_settings.phr_admin_user_password = phr_json[tenant_settings.KEY_CDR_PHR_ADMIN_USER_PASSWORD]
        elif section == 'iam.config':
            tenant_settings.parent_org_name = config_json[tenant_settings.KEY_IDM_PARENT_ORG_NAME]
        elif section == 'onboard.config':
            tenant_settings.new_tenant_org_name = config_json[tenant_settings.KEY_ONBOARD_TENANT_ORG_NAME]
            tenant_settings.customer_org_name = config_json[tenant_settings.KEY_ONBOARD_CUST_ORG_NAME]
            tenant_settings.hcadmin_login_to_assign = config_json[tenant_settings.KEY_ONBOARD_HCADMIN_LOGIN]
            tenant_settings.emr_svc_acc_login_id = config_json[tenant_settings.KEY_ONBOARD_EMR_ACC_LOGIN]
            tenant_settings.ihe_svc_acc_login_id = config_json[tenant_settings.KEY_ONBOARD_IHE_ACC_LOGIN]
            tenant_settings.jobmgmt_svc_acc_login_id = config_json[tenant_settings.KEY_ONBOARD_JOBMGMT_ACC_LOGIN]
            tenant_settings.hsdp_security_pkg_rel_path = config_json[tenant_settings.KEY_ONBOARD_HSDP_SECURITY_PATH]
            tenant_settings.consumer_org_admin_login_id = config_json[tenant_settings.KEY_ONBOARD_CONSUMER_ADMIN_LOGIN]
            tenant_settings.devicedata_acc_login_id = config_json[tenant_settings.KEY_ONBOARD_DEVICEDATA_ACC_LOGIN]
            tenant_settings.hsdp_app_svcs_org_name = config_json[tenant_settings.KEY_ONBOARD_HSDPAPP_ORG_NAME]
            tenant_settings.consumer_phr_org_name = config_json[tenant_settings.KEY_ONBOARD_CONSUMER_ORG_NAME]
            tenant_settings.hsdp_app_svcs_login_id = config_json[tenant_settings.KEY_ONBOARD_HSDP_APP_ORG_LOGIN]
        elif section == 'migrate.config':
            tenant_settings.migrate_tenant_org_name = config_json[tenant_settings.KEY_ONBOARD_TENANT_ORG_NAME]
            tenant_settings.datashare_host = config_json[tenant_settings.KEY_ONBOARD_DATASHARE_HOST]
        elif section == "rbac.db":
            tenant_settings.dbname = config_json[tenant_settings.KEY_DB_NAME]
            tenant_settings.dbhost = config_json[tenant_settings.KEY_HOST]
            tenant_settings.dbport = config_json[tenant_settings.KEY_DB_PORT]
            tenant_settings.dbusername = config_json[tenant_settings.KEY_DB_USERNAME]
            tenant_settings.dbpassword = config_json[tenant_settings.KEY_DB_PASSWORD]
        elif section == "share.config":
            tenant_settings.isshareupdate = config_json[tenant_settings.KEY_SHARE_ORG_UPDATE]


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Script to create Program Organization structure.')
    parser.add_argument("-fcon", "--fconf", action="store", dest="foundation_config",
                        help="Path of the Foundation Configuration yaml file containing server info.")
    parser.add_argument("-c", "--config", action="store", dest="configfile",
                        help="Path of the yaml configuration file containing server info, db details etc.")
    parser.add_argument("-a", "--action", action="store", dest="action_name",
                        choices=['onboard', 'migrate', 'wipeout', 'create-fhir-org'],
                        help="Script phrase which needs to execute.")
    # On Board Tenant Input
    parser.add_argument("-org", "--organization", action="store", dest="parent_org",
                        help="Specify whether the customer org is existing in IAM or to be created afresh. "
                             "Possible values: new/existing.")
    # On Board Tenant Input
    parser.add_argument("-t", "--tenanttype", action="store", dest="tenant_type",
                        help="Type of Tenant - EHR or PHR. Possible values: EHR/PHR")
    # Data Wipe out input
    parser.add_argument("-o", "--operation", action="store", dest="operation", choices=['cleanup', 'backup'],
                        help="Operation to perform. Possible values - cleanup/backup")
    # Log input
    parser.add_argument('-log', '--log-level', '--loglevel', action='store', dest='log_level',
                        help='logging level for the script, input is case sensitive',
                        choices=['DEBUG', 'INFO', 'WARNING', 'ERROR', 'CRITICAL'], default='DEBUG',
                        required=False)
    parser.add_argument('-con', '--console', help='log to screen only', action='store_true',
                        dest='console_only', required=False)
    parser.add_argument('-fil', '--file', help='log to file only', action='store_true',
                        dest='file_only', required=False)

    options = parser.parse_args()

    if options.foundation_config is None:
        parser.error("Mandatory argument '--setting' is missing")
    if options.configfile is None:
        parser.error("Mandatory argument '--config' is missing")

    log_file_dict = {'onboard': 'onboard_tenant.py', 'migrate': 'migrate_tenant.py', 'wipeout': 'wipe_out_tenant.py',
                     'create-fhir-org': 'create_fhir_org.py'}
    try:
        os.makedirs(tenant_settings.output_dir_path_rel)
    except OSError as ex:
        if ex.errno != errno.EEXIST:
            raise
    log_file_name = get_log_name(tenant_settings.output_dir_path_rel + log_file_dict[options.action_name])
    logger = configure_logging(loglevel=options.log_level, logfilename=log_file_name,
                               consoleonly=options.console_only, fileonly=options.file_only,
                               scriptname=log_file_dict[options.action_name])

    load_foundation_config(options.foundation_config)
    load_config(options.configfile)

    if options.action_name == 'onboard':
        if options.parent_org is None:
            parser.error("Mandatory argument '--organization' is missing")
        if options.tenant_type is None:
            parser.error("Mandatory argument '--tenanttype' is missing")
        onboard_tenant(options.parent_org , options.tenant_type , logger)
    elif options.action_name == 'migrate':
        migrate_tenant(logger)
    elif options.action_name == 'wipeout':
        if options.operation is None:
            parser.error("Mandatory argument '--operation' is missing")
        wipe_out_tenant(options.operation, logger)
    elif options.action_name == 'create-fhir-org':
        if options.tenant_type is None:
            parser.error("Mandatory argument '--tenanttype' is missing")
        tenant_type = options.tenant_type.lower()
        if tenant_type == 'ehr':
            cdr_fhir_host = tenant_settings.cdr_fhir_host_ehr
            tenant_settings.fhir_admin_user_id = tenant_settings.ehr_hcadmin_user_id
            tenant_settings.fhir_admin_user_password = tenant_settings.ehr_hcadmin_user_password
        elif tenant_type == 'phr':
            cdr_fhir_host = tenant_settings.cdr_fhir_host_phr
            tenant_settings.fhir_admin_user_id = tenant_settings.phr_admin_user_id
            tenant_settings.fhir_admin_user_password = tenant_settings.phr_admin_user_password
        else:
            raise ValueError('Tenant type is invalid : ' + options.tenant_type)
        create_fhir_org(logger, tenant_settings.full_qualified_org_name, cdr_fhir_host)
