import os
import yaml
import subprocess
import sys


def cfLogout():
    p = subprocess.Popen(["cf", "logout"], shell=True, stdout=subprocess.PIPE).communicate()[0]
    print p


def cfLogin(api, space, org):
    command = ["cf", "login", "-a", api,
               "-u", os.environ['CF_POC_USERNAME'],
               "-p", os.environ['CF_POC_PASSWORD'],
               "-o", org,
               "-s", space]
    p = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE).communicate()[0]
    print p


# Generate the Config File
def ConfigFileCreator(workspace):
    os.chdir(workspace + "\\CI\\PCF\\DEV\\utils\\")
    confgFileCreation = ["python", "populate_config.py",
                         "-p", "../configurations/generic_config/POC_parametrized_autoConfig.yml.j2",
                         "-i", "../configurations/generic_config/input_to_config.yml"]
    FileCreation = subprocess.Popen(confgFileCreation, shell=True, stdout=subprocess.PIPE).communicate()[0]
    print FileCreation


# Generate the Manifest File
def manifestFileCreator(workspace):
    os.chdir(workspace + "\\CI\\PCF\\DEV\\")
    manifestFileCreation = ["python", "create-manifests",
                            "-c", "./configurations/generic_config/POC_parametrized_autoConfig.yml",
                            "-i", "generic_templates",
                            "-o", "generic_manifest",
                            "--merge"]
    manifestCreation = subprocess.Popen(manifestFileCreation, shell=True, stdout=subprocess.PIPE).communicate()[0]
    print manifestCreation


# Create the Services
def serviceCreationModule():
    serviceCreation = ["python", "create-services",
                       "-c", "./configurations/generic_config/POC_parametrized_autoConfig.yml"]
    serviceCreationOut = subprocess.Popen(serviceCreation, shell=True, stdout=subprocess.PIPE).communicate()[0]
    print serviceCreationOut


# Push the Services
def pushService(workspace, deploymentType):
    # Which all services to be deployed ??
    # In Case of Nightly Build -> All service
    if (deploymentType == "NIGHTLY"):
        manifestFiles = ["master_manifest.yml"]
    # In Case of Dev Build -> Only Services which are changed
    else:
        # !!!!!!!!!!!!!!!!!!!!!!!!!!   How to get the manifest files only for changed micro service  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        # Solution :
        #   1. read all the manifest file and fetch the path.
        #   2. If path matches with the available binaries add to the manifest file list
        #       if binary file exists, it means that the module has some change- if no change this binary won't be available


        # 1. Read the manifest files :
        manifestFileList = os.listdir(workspace + "\CI\PCF\DEV\manifests\generic_manifest\\")

        manifestFiles = []
        binaryDir = workspace + "\CI\PCF\DEV\\binary"
        for manifestFile in manifestFileList:
            try:
                if (manifestFile != "master_manifest.yml"):
                    manifest = open(workspace + "\CI\PCF\DEV\manifests\generic_manifest\\" + manifestFile)
                    manifestYML = yaml.load(manifest)
                    path = manifestYML['applications'][0]['path']
                    pathLoc = path.split("binary", 1)[1]

                    # Location of the binary as specified in the manifest file
                    binaryLoc = binaryDir + pathLoc

                    # 2. Is binary Exists ? - ie, Change happened to this code - Then add to manifest file list to push
                    if (os.path.isfile(binaryLoc)):
                        manifestFiles.append(manifestFile)
            except:
                # In Case of base_manifest file , exception will be raised
                print
        print "##############--------- Starting CF Push ---------##############"
    for manifestFile in manifestFiles:
        manifestLoc =  workspace + "\CI\PCF\DEV\manifests\generic_manifest\\" + manifestFile

        #get the service name and delete if exists
        manifest = open(manifestLoc)
        manifestYML = yaml.load(manifest)
        serviceName = manifestYML['applications'][0]['name']

        cfDelete = ["cf", "delete",
                           "-f", "-r",serviceName]
        cfDeleteLog = subprocess.Popen(cfDelete, shell=True, stdout=subprocess.PIPE).communicate()[0]
        print cfDeleteLog

        serviceCreation = ["cf", "push",
                           "-f",manifestLoc]
        serviceCreationOut = subprocess.Popen(serviceCreation, shell=True, stdout=subprocess.PIPE).communicate()[0]
        print serviceCreationOut
        if serviceCreationOut.lower().__contains__("failed"):
            sys.exit(-1)


def main():
    workspace = os.environ['WORKSPACE']
    deploymentType = os.environ['deploymentType']

    # Generate Config file from template
    ConfigFileCreator(workspace)

    # Read the Config file
    manifestfile = workspace + "\CI\PCF\DEV\configurations\generic_config\POC_parametrized_autoConfig.yml"
    manifest = open(manifestfile)
    manifestYML = yaml.load(manifest)

    # Get the required values
    space = manifestYML['space_name']
    org = manifestYML['org_name']
    api = "https://" + manifestYML['api_host']

    # Generate manifest files from template
    manifestFileCreator(workspace)

    # Log-out from existing cf-target (Optional)
    cfLogout()

    # Log-in to CF - ORG and Space as given in the input file
    cfLogin(api, space, org)
    serviceCreationModule()

    # Log-in to CF - ORG and Space as given in the input file (Optional - may be helful to reduce the chance of getting cf timeout)
    cfLogin(api, space, org)
    pushService(workspace, deploymentType)


if __name__ == "__main__":
    main()