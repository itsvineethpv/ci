HSDP Deployment template for Cloud Foundry: Environment configuration files
===========================================================================

Environment configuration files should define and capture all the information
required to provision services and generate manifest files for a specific
deployment in Cloud Foundry.  There is also a fully commented example
configuration file in this directory for the TL;DR crowd.

General Settings
----------------

General environment wide configuration settings are at the top level keys in
the configuration file and should look something like this:

```
org_name: hsdp-deployment-test
space_name: ep-test-space
api_host: api.cloud.pcftest.com
login_host: login.cloud.pcftest.com
domain: cloud.pcftest.com
version: "1.8.0.0"
blue_green: blue
default_buildpack: https://github.com/cloudfoundry/java-buildpack#v3.7
```

Most of these settings are generally required.  The exceptions are `version`
and ``blue_green``.  A common pattern is to use the version number in the
application manifest templates as part of the application name, so if you are
using this naming pattern you can specify version here.  Another common pattern
is to do blue/green-type deployments to eliminate or minimize downtime.  If you
are using you pattern you can specify if the deployment is to be a blue or
green variant.  Again this would be used as part of the application name when
the application is pushed to Cloud Foundry.

The only other item that is should need a little explaining is the
``default_buildpack`` option.  This is a convenience feature.  If you have
a large number of applications that make up your deployment and all use
the same buildpack you can specify it here.  This will be inherited by all
applications that do not specify a buildpack in the application section.
The application-specific setting will always take precedence over the
global one.

Services Section
----------------

All service configuration should be nested inside the *services* key, which
specifies all of the services the applications in the deployment need in
order to function.  These can be either managed (by a service broker), user-
provided, or existing.  The `create-services` tool provided in this repo will
use the information in the services section to provision all the services in
the services section of the configuration.  And the `create-manifests` tool
will use this information to render manifest templates into valid manifest
files for a specific environment.

Services that use a service broker are always preferred since they typically
allow for self service capabilities that allow developers to use a service in
exactly the same way that a production service will use the service.  Managed
services can be defined in the configuration file like this:

```
services:
  application_db:
    service_name: application_db
    service_type: managed
    broker_name: hsdp-rds
    plan_name: mysql-medium-dev
    optional_params:
      DBName: bldp
      AllocatedStorage: 50
      DBParameterGroupName: hsdpmysql56-utf8x
```

In the above example we have a service named ``application_db`` defined.  It
is a managed service that uses the *hsdp-rds* service broker and the service
plan called mysql-medium-dev.  The hsdp-rds broker also supports some optional
parameters that we passed in.  The values for these are completely dependent
upon the service broker itself and will vary from broker to broker.  The above
parameters are all valid optional parameters for the hsdp-rds service broker.
In this case we are changing the amount of storage that the database will be
deployed with, the name of the database itself, and the RDS database parameter
group name.  This is a good example of why it is important for developers to
specify these values for the production configuration file.  Operations would
have no way of knowing that you need a special parameter group or a special
database name, or even the amount of storage you specified your application
will need.

User-provided services are not provisioned by a service broker and only
consist of optional parameters.  Common examples are enabling New Relic for
the Java Buildpack, or enabling a log drain to a third party destination
for application logs.  Here is a sample user-provided service:

```
services:
  newrelic:
    service_name: newrelic
    service_type: user-provided
    optional_params:
      licenseKey: 11111111111111111111111111111111111
```

In this example we can see that the service_type has been set to
'user-provided' instead of 'managed'.  The only other value we have here
is a license key that is passed along to the application container as
an environment variable.  This is actually a valid configuration (if
you use a real license key and the not fake one I used in the example)
to enable New Relic in the Java Buildpack.  If you have ever created a
user-provided service with the Cloud Foundry command line tools you know
that you can essential pass in any valid JSON with the `cf cups` command.
You can pass that same data in the optional_params key, albeit in in YAML
form instead of JSON.

The final type of service that can be defined is called 'existing'.  If the
service_type is set to existing then it will be ignored by the create-services
tool.  The service name will still be used to render manifest templates into
valid manifest files for a specific environment so they will still need to be
provided as part of the configuration.  Existing services look like this:

```
services:
  application_db:
    service_name: application_db
    service_type: existing
    broker_name: hsdp-rds
    plan_name: mysql-medium-dev
    optional_params:
      DBName: bldp
      AllocatedStorage: 50
      DBParameterGroupName: hsdpmysql56-utf8x
```

Technically it does not hurt to provide all the other information, but if the
service_type is set to existing it will just be ignored by the create-services
tool.

Base Section
------------

The base section of the configuration file should include settings that
will be used to render what we call the base manifest file.  This is a
special manifest that really just has deployment-wide settings in it.
It will be inherited by all other manifest files.  Usually there are
some common environment variables that you would want all applications to
have for a given environment.  These variables can be specified here and
will ultimately be merged with the environment variables section of each
application manifest.

```
base:
  env:
    SPRING_PROFILES_ACTIVE: pcftest
    env_var1: all_apps_need_this1
    env_var2: all_apps_need_this2
```

The base template itself will have default values for other things like number
of instances, memory allocation, etc.  There are some differences in the way
certain settings are merged together.  In some cases values will be merged and
in others overridden.  See the templates directory README.md for more details
on that topic.

Application-Specific Environment Settings
-----------------------------------------

There are no remaining default sections of the configuration format.  Anything
else is considered an application-specific configuration.  This is where you
would store all the settings for an application that your manifest templates
expect to have for proper rendering.  Here are a couple of examples:

```
web_application:
  env:
    JBP_CONFIG_TOMCAT: '{tomcat: { version: 8.0.+ }}'
    SPRING_PROFILES_ACTIVE: sharding_pcftest

web_application_worker:
  instances: 1
  env:
    JBP_CONFIG_OPEN_JDK_JRE: '{jre: { version: 1.7.0_+ }}'
    SUBJECT_BATCH_RUN_AT_INSTANT: true
```

In the above (contrived) example we have set the Tomcat version to 8 for our
web_application and the spring profile to sharding_pcftest.  When the manifest
files are rendered for this environment the env sections from base and
web_application will be merged.  You can see that we have a conflict with one
of the keys here: SPRING_PROFILES_ACTIVE.  In this case the value will be
sharding_test since the application-specific value will override the default
value provided in the base section.

For the web_application_worker section we have some environment variables and
a new setting called instances.  In this case we have also added instances.
This is an example of extending the variables for a single application in
the deployment.  In order for this to be effective the manifest template
would also have to account for it.  Sometimes it will make sense to make
a specific setting managed in the environment configuration and sometimes
it will make sense to just have it hard coded into the template itself.
Don't just do one or the other without thinking through whether or not
you need to make it a variable.  There are times when an application will
only ever have a single value for a setting regardless of environment, in
these cases it would be easier to manage as a value in the actual template
as opposed to a value that has to be copy and pasted from environment
to environment.