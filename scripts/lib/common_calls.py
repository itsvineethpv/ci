#!/usr/bin/python
# Name: common_calls.py
# Author: Amit Kumar, amit.k@philips.com
# Description: This script contains common functions used across the package
# Created: 10th October 2016
# Modified: 14th October 2016
# #
# Copyright Philips India, 2016
#

import re
import os
import json

import collections
from collections import OrderedDict

import yaml
import random
import logging
import psycopg2
import psycopg2.extras
import requests
from requests.auth import HTTPBasicAuth
from yaml.representer import Representer

from create_hmac_signature import *
from lib import tenant_settings


def rest_call(url, verb, headers_param, body):
    resp = ""
    response_body = ""
    if headers_param is not None:
        if verb == tenant_settings.HTTP_VERB_GET:
            resp = requests.request(method=verb, url=url, headers=headers_param)
        elif verb == tenant_settings.HTTP_VERB_DELETE:
            if body is not None:
                resp = requests.request(method=verb, url=url, json=body, headers=headers_param)
            else:
                resp = requests.request(method=verb , url=url , headers=headers_param)
        elif (verb == tenant_settings.HTTP_VERB_POST) and (body is not None):
            resp = requests.request(method=verb, url=url, json=body, headers=headers_param)
        elif (verb == tenant_settings.HTTP_VERB_PUT) and (body is not None):
            resp = requests.request(method=verb, url=url, json=body, headers=headers_param)
        else:
            print "Method not supported.\n"
        if resp is not None:
            response_body = analyze_response(resp, verb)
    else:
        print "Headers is required but is Empty.\n"
    return response_body


def analyze_response(response, verb):
    # print "Response analysis - Start."
    resp_string = ""
    resp_json = ""
    # global IS_OPERATION_SUCCESS
    tenant_settings.IS_OPERATION_SUCCESS = False
    res_heads = response.headers
    if tenant_settings.HEADER_KEY_CONTENT_TYPE in res_heads:
        content_type = response.headers[tenant_settings.HEADER_KEY_CONTENT_TYPE]
        # print "Response content-type : " + content_type
        if content_type.find('text/html') != -1:
            resp_string = response.text
        elif content_type.find('application/json') != -1:
            resp_json = response.json()
            resp_string = json.dumps(resp_json)
        elif content_type.find('application/json+fhir') != -1:
            resp_json = response.json()
            resp_string = json.dumps(resp_json)
    is_success = False
    if verb == tenant_settings.HTTP_VERB_GET:
        if response.status_code == 200:
            # print verb + " method returned success response" + resp_string
            is_success = True
        else:
            print verb + " method returned failure response " + resp_string
            remarks = resp_string
    elif verb == tenant_settings.HTTP_VERB_POST:
        if response.status_code == 201 or response.status_code == 200:
            # print verb + " method returned success response" + resp_string
            is_success = True
        else:
            print verb + " method returned failure response " + resp_string
            remarks = resp_string
    elif verb == tenant_settings.HTTP_VERB_DELETE:
        if response.status_code == 200:
            # print verb + " method returned success response" + resp_string
            is_success = True
        else:
            print verb + " method returned failure response " + resp_string
            remarks = resp_string
    elif verb == tenant_settings.HTTP_VERB_PUT:
        if response.status_code == 200:
            # print verb + " method returned success response" + resp_string
            is_success = True
        else:
            print verb + " method returned failure response " + resp_string
            remarks = resp_string
    if is_success:
        tenant_settings.IS_OPERATION_SUCCESS = True
    # print "Response analysis - End."
    return resp_json


def connect_db(logger):
    try:
        conn_string = "host=" + tenant_settings.dbhost + " port=" + str(tenant_settings.dbport) \
                      + " dbname=" + tenant_settings.dbname + " user=" + tenant_settings.dbusername + " password=" + tenant_settings.dbpassword
        logger.info("Connecting to database :: host = %s, database name = %s and user = %s", tenant_settings.dbhost, tenant_settings.dbname, tenant_settings.dbusername)
        database_connection = psycopg2.connect(conn_string)
        return database_connection
    except Exception as e:
        logger.error(e)
        raise ValueError("Error During Data Base Connection")


def get_session_token_only_header():
    session_token = get_session_token()
    sess_token_only_header = {
        'x-token': session_token,
        'Content-Type': 'application/json'
    }
    return sess_token_only_header


def get_sesstoken_api_version_header():
    session_token = get_session_token()
    get_all_header = {
        'x-token': session_token,
        'api-version': '1',
        'Content-Type': 'application/json'
    }
    return get_all_header


def get_accesstoken_apisign_header(logger):
    oauth_token = get_oauth_token(logger)
    apisign_header = get_api_signing_header(hmac_id=tenant_settings.idm_hmac_id ,
                                            hmac_secret=tenant_settings.idm_hmac_secret)
    access_token_header = {
        'x-token': oauth_token
    }
    access_token_header.update(apisign_header)
    basic_header = form_basic_headers()
    access_token_header.update(basic_header)
    return access_token_header


def get_cdr_fhir_header(logger):
    access_token_apisign_header = get_accesstoken_apisign_header(logger)
    cdr_header = {
        'x-application-key': 'cdr-app'
    }
    cdr_header.update(access_token_apisign_header)
    logger.info ('CDR FHIR Information : %s', cdr_header)
    return cdr_header


def get_session_token():
    session_url = tenant_settings.iam_host + '/security/authentication/session/login'
    session_header = get_api_signing_header(hmac_id=tenant_settings.idm_hmac_id ,
                                            hmac_secret=tenant_settings.idm_hmac_secret)
    basic_header = form_basic_headers()
    session_header.update(basic_header)
    session_request = {
        'loginId': tenant_settings.idm_enterprise_user_login,
        'password': tenant_settings.idm_enterprise_user_password
    }
    session_token_response = rest_call(url=session_url , verb=tenant_settings.HTTP_VERB_POST ,
                                       headers_param=session_header , body=session_request)
    exchange_key = 'exchange'
    authentication_key = 'authentication'
    token_id_key = 'tokenId'
    token_value = ''
    if (session_token_response is not None) and (exchange_key in session_token_response):
        exchange_json = session_token_response[exchange_key]
        if (exchange_json is not None) and (authentication_key in exchange_json):
            authentication_json = exchange_json[authentication_key]
            if (authentication_json is not None) and (token_id_key in authentication_json):
                token_value = authentication_json[token_id_key]
    return token_value


def get_oauth_token(logger):
    logger.debug('Getting oauth token from %s', tenant_settings.iam_host)
    oauth_token = ''
    oauth_url = tenant_settings.iam_host + '/authorize/oauth2/token'
    oauth_basic_auth = HTTPBasicAuth(username=tenant_settings.oauth_client_id ,
                                     password=tenant_settings.oauth_client_secret)
    oauth_header = {
        tenant_settings.HEADER_KEY_CONTENT_TYPE: 'application/x-www-form-urlencoded',
        tenant_settings.FR_HEADER_ACCEPT: 'application/json'
    }
    oauth_request_body = 'grant_type=password&username=' + tenant_settings.fhir_admin_user_id + \
                         '&password=' + tenant_settings.fhir_admin_user_password
    oauth_response = requests.post(url=oauth_url, data=oauth_request_body, headers=oauth_header, auth=oauth_basic_auth)
    if (oauth_response is not None) and (oauth_response.status_code == 200):
        print 'Request for oauth token is successful'
        oauth_response = oauth_response.json()
        access_token_key = 'access_token'
        if access_token_key in oauth_response:
            oauth_token = oauth_response[access_token_key]
    else:
        logger.error('Request for oauth token returned failure response : %s', oauth_response.json())
    return oauth_token


def get_group_by_name(group_name, org_id_param, logger):
    logger.debug('Retrieving Group : %s and orgId : %s', group_name, org_id_param)
    get_grp_url = tenant_settings.idm_host + '/authorize/identity/Group?name=' + group_name +\
                  '&orgID=' + org_id_param
    get_grp_header = get_sesstoken_api_version_header()
    get_grp_response = rest_call(url=get_grp_url , verb=tenant_settings.HTTP_VERB_GET ,
                                 headers_param=get_grp_header , body=None)
    return get_grp_response


def get_all_groups(org_id, logger):
    logger.debug('Get all groups for an org : %s', org_id)
    get_all_grps_url = tenant_settings.idm_host + '/authorize/identity/Group?orgID=' + org_id
    get_all_header = get_sesstoken_api_version_header()
    get_all_grps_response = rest_call(url=get_all_grps_url , verb=tenant_settings.HTTP_VERB_GET ,
                                      headers_param=get_all_header , body=None)
    # extract the group details from response
    groups_list = []
    entry_key = 'entry'
    group_name_key = 'groupName'
    group_id_key = '_id'
    if (get_all_grps_response is not None) and (entry_key in get_all_grps_response):
        entries = get_all_grps_response[entry_key]
        for entry in entries:
            resource_key = 'resource'
            if (entry is not None) and (resource_key in entry):
                resource = entry[resource_key]
                if (resource is not None) and (group_name_key in resource) and (group_id_key in resource):
                    group_name = resource[group_name_key]
                    group_id = resource[group_id_key]
                    group_info_dict = {
                        tenant_settings.GROUP_ID_KEY: group_id,
                        tenant_settings.GROUP_NAME_KEY: group_name
                    }
                    groups_list.append(group_info_dict)
    logger.debug('Got all the groups for orgId : %s Groups : %s', org_id, groups_list)
    return groups_list


def get_all_users(group_id_param, logger):
    logger.debug('Get all users in group : ' + group_id_param)
    get_users_url = tenant_settings.idm_host + '/security/users?groupId=' + group_id_param + \
                    '&pageSize=20000'
    get_all_header = get_session_token_only_header()
    get_users_response = rest_call(url=get_users_url , verb=tenant_settings.HTTP_VERB_GET ,
                                   headers_param=get_all_header , body=None)
    exchange_key = 'exchange'
    users_key = 'users'
    users = []
    if (get_users_response is not None) and (exchange_key in get_users_response):
        exchange = get_users_response[exchange_key]
        if (exchange is not None) and (users_key in exchange):
            users = exchange[users_key]
            logger.debug('All users in group ' + group_id_param + ' are ::: ' + str(users))
    return users


def delete_idm_group(org_id, group_id, logger):
    logger.debug('Delete an IDM group : ' + group_id)
    delete_grp_url = tenant_settings.idm_host + '/security/organizations/' + org_id + '/groups/' + group_id
    delete_headers = get_sesstoken_api_version_header()
    resp = rest_call(url=delete_grp_url , verb=tenant_settings.HTTP_VERB_DELETE , headers_param=delete_headers , body=None)
    if (resp is not None) and ('responseCode' in resp):
        response_code = resp['responseCode']
        if response_code == '200':
            logger.debug('Delete Group - Operation Successful')
            tenant_settings.IS_OPERATION_SUCCESS = True
        else:
            logger.error('Delete Group - Operation Failed')
            tenant_settings.IS_OPERATION_SUCCESS = False
    else:
        logger.error('Delete Group - Operation Failed')
        tenant_settings.IS_OPERATION_SUCCESS = False
    logger.debug('Exiting operation delete_idm_group()')


def remove_users_from_group(org_id, group_id_param, user_ids, logger):
    logger.debug('Operation Remove Users from Group - Started')
    remove_users_url = tenant_settings.idm_host + '/security/organizations/' + org_id + '/groups/' \
                       + group_id_param + '/users'
    remove_users_header = get_session_token_only_header()
    remove_users_request = {
        'userIds': user_ids
    }
    remove_users_response = rest_call(url=remove_users_url , verb=tenant_settings.HTTP_VERB_DELETE ,
                                      headers_param=remove_users_header , body=remove_users_request)
    if (remove_users_response is not None) and ('responseCode' in remove_users_response):
        response_code = remove_users_response['responseCode']
        if response_code == '200':
            logger.debug('Remove Users from Group - Operation Successful')
            tenant_settings.IS_OPERATION_SUCCESS = True
        else:
            logger.error('Remove Users from Group - Operation Failed')
            tenant_settings.IS_OPERATION_SUCCESS = False
    else:
        logger.error('Remove Users from Group - Operation Failed')
        tenant_settings.IS_OPERATION_SUCCESS = False
    logger.debug('Operation Remove Users from Group - Finished')
    return remove_users_response


def assign_user_to_group(user_ids_arr , group_id, logger):
    assign_url = tenant_settings.idm_host + '/authorize/identity/Group/' + group_id + '/$add-members'
    assign_user_header = get_api_signing_header(hmac_id=tenant_settings.idm_hmac_id ,
                                                hmac_secret=tenant_settings.idm_hmac_secret)
    basic_header = form_basic_headers()
    assign_user_header.update(basic_header)
    more_header = get_sesstoken_api_version_header()
    assign_user_header.update(more_header)
    assign_user_request = {
        'resourceType': 'Parameters',
        'parameter': [
            {
                'name': 'UserIDCollection',
                'references': user_ids_arr
            }
        ]
    }
    assign_user_response = rest_call(url=assign_url , verb=tenant_settings.HTTP_VERB_POST ,
                                     headers_param=assign_user_header , body=assign_user_request)
    if (assign_user_response is None) or (len(assign_user_response) == 0):
        tenant_settings.IS_OPERATION_SUCCESS = True
    elif len(assign_user_response) > 0:
        logger.error('Assign Users to Group - Operation Failed')
        tenant_settings.IS_OPERATION_SUCCESS = False
    return assign_user_response


def create_child_org(parent_org_id, child_org_name, logger):
    logger.debug('Creating child organization ' + child_org_name + ' in parent org : ' + parent_org_id)
    create_child_org_url = tenant_settings.idm_host + '/security/organizations/' + \
                           parent_org_id + '/childorganizations'
    child_org_header = get_session_token_only_header()
    child_org_request = {
        'name': child_org_name
    }
    create_child_org_response = rest_call(url=create_child_org_url , verb=tenant_settings.HTTP_VERB_POST ,
                                          headers_param=child_org_header , body=child_org_request)
    if (create_child_org_response is not None) and ('responseCode' in create_child_org_response):
        response_code = create_child_org_response['responseCode']
        if response_code == '4008':
            logger.info('Organization already exists. Fetching it ...')
            # fetch the org by name
            create_child_org_response = get_org_by_name(child_org_name, logger)
            if 'exchange' in create_child_org_response:
                exchange = create_child_org_response['exchange']
                if len(exchange) > 1:
                    logger.warning('There are more than one org with same name.')
                    create_child_org_response = {
                        'responseMessage': 'Success',
                        'responseCode': '200',
                        'exchange': exchange[0]
                    }
                else:
                    resp_string = json.dumps(create_child_org_response)
                    resp_string = resp_string.replace('[', '')
                    resp_string = resp_string.replace(']', '')
                    create_child_org_response = json.loads(resp_string)
    return create_child_org_response


def create_idm_group(org_id_param, group_name, logger):
    logger.debug('Creating IDM Group with name ' + group_name + ' in org : ' + org_id_param)
    create_grp_url = tenant_settings.idm_host + '/security/organizations/' + org_id_param + '/groups'
    create_grp_header = get_session_token_only_header()
    create_grp_request = {
        'groupName': group_name
    }
    create_grp_response = rest_call(url=create_grp_url , verb=tenant_settings.HTTP_VERB_POST ,
                                    headers_param=create_grp_header , body=create_grp_request)
    if (create_grp_response is not None) and ('responseCode' in create_grp_response):
        response_code = create_grp_response['responseCode']
        if response_code == '4007':
            logger.info('Group already exists. Fetching it ...')
            # fetch the group by name
            get_grp_resp = get_group_by_name(group_name, org_id_param, logger)
            group_uuid = ''
            if get_grp_resp is not None:
                resp_string = json.dumps(get_grp_resp)
                resp_string = resp_string.replace('[', '')
                resp_string = resp_string.replace(']', '')
                get_grp_resp = json.loads(resp_string)
                group_uuid = get_grp_resp['entry']['resource']['_id']
            if group_uuid != '':
                create_grp_response = {
                    'exchange': {
                        'groupName': group_name,
                        'groupID': group_uuid
                    }
                }
    return create_grp_response


def generate_random_password(choice_param):
    if choice_param is not None:
        password = "".join(random.choice(choice_param)
                           for x in range(6)). \
            replace(".", "@")
        x = "".join(random.choice("ABCDEUVWXYZ") for x in range(2))
        y = "".join(random.choice("!@#") for x in range(1))
        z = "".join(random.choice("0123456789") for x in range(1))
        password = password + x + y + z
    return password


def create_idm_user(user_login, org_id, user_fname, user_mname, user_lname, logger):
    logger.info('Creating user ' + user_login + ' in org : ' + org_id)
    create_user_url = tenant_settings.idm_host + '/security/organizations/' + org_id + '/users'
    user_password = generate_random_password(user_login)
    create_user_request = {
        'loginId': user_login,
        'password': user_password,
        'profile': {
            'givenName': user_fname,
            'familyName': user_lname,
            'middleName': user_mname,
            'contact': {
                'emailAddress': user_login
            }
        }
    }
    create_user_header = get_session_token_only_header()
    create_user_resp = rest_call(url=create_user_url , verb=tenant_settings.HTTP_VERB_POST ,
                                 headers_param=create_user_header , body=create_user_request)
    if create_user_resp is not None and 'responseCode' in create_user_resp:
        response_code = create_user_resp['responseCode']
        if response_code == '200':
            uuid = create_user_resp['exchange']['userUUID']
            log_msg = 'User created successfully. LoginId : ' + user_login + ' Password : ' + user_password +\
                      ' Iam-Id : ' + uuid
            logger.info(log_msg)
    return {'response': create_user_resp, 'password': user_password}


def get_org_by_name(org_name, logger):
    logger.debug('Get Org By Name - OrgName : ' + org_name)
    get_org_url = tenant_settings.idm_host + '/security/organizations?organizationName=' + org_name
    get_org_header = get_session_token_only_header()
    get_org_resp = rest_call(url=get_org_url , verb=tenant_settings.HTTP_VERB_GET ,
                             headers_param=get_org_header , body=None)
    return get_org_resp


def get_user_by_loginid(login_id, logger):
    logger.debug('Getting User by LoginId - ' + login_id)
    get_user_url = tenant_settings.idm_host + '/security/users?loginId=' + login_id
    get_user_header = get_session_token_only_header()
    get_user_resp = rest_call(url=get_user_url , verb=tenant_settings.HTTP_VERB_GET ,
                              headers_param=get_user_header , body=None)
    return get_user_resp


def get_uuid_from_org_response(org_response, logger):

    orgid = ''
    if (org_response is not None) and ('exchange' in org_response):
        orgid = org_response['exchange'][0]['organizationId']
    else:
        logger.error('OrgId not found in response')
    return orgid


def get_user_uuid_4m_response(create_user_response, logger):
    logger.debug('Getting uuid from create user response')
    local_user_uuid = ''
    if (create_user_response is not None) and ('responseCode' in create_user_response):
        response_code = create_user_response['responseCode']
        if response_code == '200':
            local_user_uuid = create_user_response['exchange']['userUUID']
    else:
        logger.error('Response code not found in response body')
    return local_user_uuid


def create_shareable_group(server_host, group_name, group_id, org_name, org_id, logger):
    logger.info('Creating ShareableGroup with name : %s on server : %s',group_name, server_host)
    shareable_grp_url = 'https://' + server_host + '/authorize/data/ShareableGroup'
    sg_header = get_accesstoken_apisign_header(logger)
    sg_request = {
        'resourceType': 'ShareableGroup',
        'groupName': group_name,
        'groupId': group_id,
        'organizationName': org_name,
        'organizationId': org_id
    }
    sg_resp = rest_call(url=shareable_grp_url , verb=tenant_settings.HTTP_VERB_POST , headers_param=sg_header ,
                        body=sg_request)
    logger.debug('Create ShareableGroup - Operation Finished')
    return sg_resp


def check_create_shareable_grp_response(shareable_resp, logger):
    logger.debug('Checking create shareable group response ...')
    outcome = 'created'
    if shareable_resp is not None and 'issue' in shareable_resp:
        issue = shareable_resp['issue']
        if isinstance(issue, list):
            diagnostic = issue[0]['diagnostics']
            outcome = diagnostic
        else:
            diagnostic = shareable_resp['issue']['diagnostics']
        if diagnostic == 'Entity Already Exists':
            outcome = 'duplicate'
    return outcome


def form_basic_headers():
    basic_header_dict = {
        tenant_settings.FR_HEADER_ACCEPT: tenant_settings.HTTP_CONTENT_TYPE_JSON,
        tenant_settings.HEADER_KEY_CONTENT_TYPE: tenant_settings.HTTP_CONTENT_TYPE_JSON
    }
    return basic_header_dict


def configure_logging(loglevel, logfilename=None, consoleonly=False, fileonly=False, scriptname=None):
    """
    Create a logger to be used for the script.  Log file will be named after the script.  By default the script logs
    to console and the file system
    :param loglevel:
        Default log level to set. Valid choices are DEBUG, INFO, WARNING, ERROR, and CRITICAL
    :param logfilename:
        Name of the log file, defaults to file name
    :param consoleonly:
        Set to true if you only want to log to console and not to file system
    :param fileonly:
        Set to true if you only want to log to file system and not the console.
    :return:
        Return logger object
    """

    numeric_level = getattr(logging, loglevel.upper(), None)
    if not isinstance(numeric_level, int):
        raise ValueError('Invalid log level: %s' % loglevel)
    if not consoleonly and not fileonly:
        raise ValueError('Must log to the console, the filesystem or both.')
    logger = logging.getLogger(scriptname)
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    if fileonly:
        if not logfilename:
            logfilename = get_log_name(os.path.basename(__file__))
        file_handler = logging.handlers.RotatingFileHandler(filename=logfilename, maxBytes=10485760, backupCount=10)
        file_handler.setLevel(numeric_level)
        file_handler.setFormatter(formatter)
        logger.addHandler(file_handler)
    if consoleonly:
        console_handler = logging.StreamHandler()
        console_handler.setLevel(numeric_level)
        console_handler.setFormatter(formatter)
        logger.addHandler(console_handler)
    logger.setLevel(numeric_level)
    return logger


def get_log_name(filename):
    """
    Takes in the file name of a .py or .pyc file and returns the name of the log file.  It replaces the .py or .pyc
    extension with .log
    :param filename:
        Name of the file
    :return:
        Name of the log to be created
    """
    if not re.search('\.pyc$', filename):
        filename = filename.replace('.py', '.log')
    else:
        filename = filename.replace('.pyc', '.log')
    return filename


def sort_order(orders):
    orders = [{k: -i for (i, k) in enumerate(reversed(order), 1)} for order in orders]

    def process(stuff):
        if isinstance(stuff, dict):
            l = [(k, process(v)) for (k, v) in stuff.items()]
            keys = set(stuff)
            for order in orders:
                if keys.issuperset(order):
                    return OrderedDict(sorted(l, key=lambda x: order.get(x[0], 0)))
            return OrderedDict(sorted(l))
        if isinstance(stuff, list):
            return [process(x) for x in stuff]
        return stuff

    return process


def generate_yaml(filename, orgs):
    yaml.add_representer(collections.defaultdict, Representer.represent_dict)
    #custom_sort = sort_order(["org_name", "org_uuid", "sub_org","sub_org_name","sub_org_uuid", "users"])
    with open(filename, 'w') as write_to_file:
        yaml.safe_dump(orgs, write_to_file, default_flow_style=False)