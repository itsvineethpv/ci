#!/usr/bin/python
# Name: create_fhir_org.py
# Author: Amit Kumar, amit.k@philips.com
# Description: This script creates a fhir organization in Dataservices R1.8 system
# Created: 04th Nov 2016
# Modified: 04th Nov 2016
# #
# Copyright Philips India, 2016
#

import os
import os
import argparse
import logging
import logging.handlers

import subprocess
from optparse import OptionParser

import yaml

from lib.common_calls import *

logger = ''
cdr_fhir_host = ''


def generate_fhir_org_resource(org_name, org_identifier_system, org_identifier_value):
    logger.debug('Generate FHIR Org resource json - Operation Started')
    fhir_org_resource = {
        'resourceType': 'Organization',
        'identifier': [
            {
                'use': 'usual',
                'system': org_identifier_system,
                'value': org_identifier_value
            }
        ],
        'name': org_name,
        'active': 'true'
    }
    logger.debug('Finished generating org resource payload : ', fhir_org_resource)
    return fhir_org_resource


def create_org(org_resource_payload, logger):
    logger.debug('Create Fhir Organization - Operation Started')
    create_org_url = 'https://' + cdr_fhir_host + '/cdr/api/Organization'
    create_org_header = get_cdr_fhir_header(logger)
    create_org_resp = rest_call(url=create_org_url, verb=tenant_settings.HTTP_VERB_POST,
                                headers_param=create_org_header, body=org_resource_payload)
    if create_org_resp is not None:
        logger.info('Organization create response : ' + json.dumps(create_org_resp))
    logger.debug('Create Fhir Organization - Operation Finished')
    return create_org_resp


def fhir_get_id_4m_response(fhir_resp):
    org_id = ''
    if (fhir_resp is not None) and ('id' in fhir_resp):
        org_id = fhir_resp['id']
    else:
        print 'Org Id not found in fhir response'
        logger.error('Org Id not found in fhir response')
    return org_id


def get_tenant_name(full_org_name):
    tenant_org = ''
    if ':' in full_org_name:
        org_arr = full_org_name.split(':')
        arr_len = len(org_arr)
        tenant_org = org_arr[arr_len - 1]
        if tenant_org == '' or tenant_org is None:
            print 'Bad format entered for fully qualified org name'
            logger.error('Bad format entered for fully qualified org name')
            raise ValueError('Bad format entered for fully qualified org name : ' + full_org_name)
    else:
        tenant_org = full_org_name
    return tenant_org


def create_fhir_org(logger_param, full_org_name_param, cdr_fhir_host_param):
    global logger
    logger = logger_param
    global cdr_fhir_host
    cdr_fhir_host = cdr_fhir_host_param
    if (full_org_name_param == '') or (full_org_name_param is None):
        raise ValueError('Fully qualified OrgName is mandatory.')
    if (cdr_fhir_host == '') or (cdr_fhir_host is None):
        raise ValueError('CDR fhir host is mandatory.')
    logger.info('FHIR Org Creation - Operation Started.')
    iam_org_uuid = ''
    # get tenant org from the fully qualified name
    tenant_org_name = get_tenant_name(full_org_name_param)
    get_org_response = get_org_by_name(tenant_org_name, logger)
    iam_org_uuid = get_uuid_from_org_response(get_org_response, logger)
    if tenant_settings.IS_OPERATION_SUCCESS and (iam_org_uuid != ''):
        print 'Org Details : ', get_org_response
        user_in = raw_input('Is the above org correct which you want to create in fhir ? (yes/no)')
        if user_in.lower() == 'yes' or user_in.lower() == 'y':
            fhir_org_res = generate_fhir_org_resource(full_org_name_param, tenant_settings.FHIR_ID_SYSTEM,
                                                      iam_org_uuid)
            fhir_resp = create_org(fhir_org_res, logger)
            fhir_org_id = fhir_get_id_4m_response(fhir_resp)
            if tenant_settings.IS_OPERATION_SUCCESS and fhir_org_id != '':
                log_msg = 'Fhir Org created successfully - Organization ID : ' + fhir_org_id
                logger.info(log_msg)
            else:
                logger.error('Org Creation Failed - Retry Again.')
        else:
            print '\nOpted to exit the operation. Exiting.'
    logger.info('FHIR Org Creation - Operation Finished.')
