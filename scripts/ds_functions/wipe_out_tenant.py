#!/usr/bin/python
# Name: wipe_out_tenant.py
# Author: Amit Kumar, amit.k@philips.com
# Edited By: Swarit Agarwal
# Description: This script is responsible to wipe-out data from IAM instance for a Dev/Test DataServices R1.7 system
# Created: 14th October 2016
# Modified: 20th October 2016
# #
# Copyright Philips India, 2016
#

from lib.common_calls import *
from lib.create_hmac_signature import *

logger = ''
parent_org_name = ''
forgerock_adminuser = ''
forgerock_adminpass = ''
forgerock_host = ''
idm_host = ''


def get_all_users_in_org(org_id):
    logger.debug('Get all users in Org : ' + org_id)
    get_users_url = idm_host + '/security/users?organizationId=' + org_id + '&pageSize=20000'
    get_all_header = get_session_token_only_header()
    get_users_response = rest_call(url=get_users_url , verb=tenant_settings.HTTP_VERB_GET , headers_param=get_all_header , body=None)
    exchange_key = 'exchange'
    users_key = 'users'
    users = []
    if (get_users_response is not None) and (exchange_key in get_users_response):
        exchange = get_users_response[exchange_key]
        if (exchange is not None) and (users_key in exchange):
            users = exchange[users_key]
            logger.debug('All users in Org ' + org_id + ' are :: ' + str(users))
    logger.debug('Operation Finished - Get all users in Org : ' + org_id)
    return users


def get_fr_user(user_id):
    logger.debug('Get user from Forgerock for userId ' + user_id)
    get_url = forgerock_host + '/openidm/endpoint/linkedView/managed/user/' + user_id
    headers_get = form_common_fr_headers()
    get_resp = rest_call(url=get_url , verb=tenant_settings.HTTP_VERB_GET , headers_param=headers_get , body=None)
    logger.debug('Exiting operation get_fr_user()')
    return get_resp


def delete_fr_org(org_id):
    logger.debug('Delete an Organization from forgerock')
    del_org_url = forgerock_host + '/openidm/managed/organization/' + org_id
    del_headers = form_common_fr_headers()
    rest_call(url=del_org_url , verb=tenant_settings.HTTP_VERB_DELETE , headers_param=del_headers , body=None)
    logger.info('Organization ' + org_id + ' deleted')


def delete_fr_user(user_id):
    logger.debug('Delete a user from forgerock for id : ' + user_id)
    del_user_url = forgerock_host + '/openidm/managed/user/' + user_id
    del_headers = form_common_fr_headers()
    rest_call(url=del_user_url , verb=tenant_settings.HTTP_VERB_DELETE , headers_param=del_headers , body=None)
    logger.debug('Exiting operation delete_fr_user()')


def delete_fr_group(group_id):
    logger.debug('Delete a group from forgerock for id : ' + group_id)
    del_grp_url = forgerock_host + '/openidm/managed/group/' + group_id
    del_headers = form_common_fr_headers()
    rest_call(url=del_grp_url , verb=tenant_settings.HTTP_VERB_DELETE , headers_param=del_headers)
    logger.debug('Exiting operation delete_fr_group()')


def form_common_fr_headers():
    basic_headers = form_basic_headers()
    common_headers_dict = {
        tenant_settings.FR_HEADER_OPENIDM_USERNAME: forgerock_adminuser,
        tenant_settings.FR_HEADER_OPENIDM_PASSWORD: forgerock_adminpass
    }
    common_headers_dict.update(basic_headers)
    return common_headers_dict


def write_data_to_file(file_path, data_to_write):
    logger.info('Writing user data to dump file ' + file_path)
    logger.debug(data_to_write)
    try:
        with open(file_path, 'ab') as dump_file:
            dump_file.write(data_to_write)
            dump_file.write('\n')
            dump_file.write('==========================================='
                            '===================================================================')
            dump_file.write('\n')
            dump_file.close()
    except IOError:
        logger.error("Error : Can\'t find file or write data" + file_path)
    logger.debug('Finished writing user data to dump file')


def load_data():
    global parent_org_name
    global forgerock_adminuser
    global forgerock_adminpass
    global forgerock_host
    global idm_host
    parent_org_name = tenant_settings.parent_org_name
    forgerock_adminuser = tenant_settings.forgerock_adminuser
    forgerock_adminpass = tenant_settings.forgerock_adminpass
    forgerock_host = tenant_settings.forgerock_host
    idm_host = tenant_settings.idm_host


def wipe_out_tenant(operation, log):
    load_data()
    global logger
    logger = log
    logger.info('DataServices IAM Customer Data Cleanup/Backup of R1.7 - Started.')
    iam_customer_org_uuid = ''
    if operation.lower() == 'backup' or operation.lower() == 'cleanup':
        get_org_resp = get_org_by_name(parent_org_name, logger)
        iam_customer_org_uuid = get_uuid_from_org_response(get_org_resp, logger)
    if operation.lower() == 'backup':
        # create a name for the user dump file
        logger.info('Dumping user data to file - Operation Started')
        user_dump_file = tenant_settings.output_dir_path_rel + 'iam_user_dump_'
        current_timestamp = datetime.now().strftime('%Y-%m-%d %H-%M-%S')
        user_dump_file = user_dump_file + current_timestamp.replace(' ', '_') + '.txt'
        # get all users in an org
        all_users_parent_org = get_all_users_in_org(iam_customer_org_uuid)
        for user_in_org in all_users_parent_org:
            user_uuid = user_in_org[tenant_settings.USER_UUID_EXCHANGE_KEY]
            # get the user from forgerock and write the details to a file
            user_get_resp = get_fr_user(user_uuid)
            if tenant_settings.IS_OPERATION_SUCCESS:
                write_data_to_file(user_dump_file , json.dumps(user_get_resp))
        logger.info('Dumping user data to file - Operation Finished')
    elif operation.lower() == 'cleanup':
        user_in = raw_input('Have you taken user data backup before performing a cleanup ? (yes/no)')
        if user_in.lower() == 'yes' or user_in.lower() == 'y':
            logger.info('DataServices IAM Customer Data Wipe-Out - Started.')
            if iam_customer_org_uuid != '':
                # get all groups in an org
                all_groups_parent_org = get_all_groups(iam_customer_org_uuid, logger)
                for group in all_groups_parent_org:
                    users_array = []
                    group_id = group[tenant_settings.GROUP_ID_KEY]
                    users_in_group = []
                    if tenant_settings.IS_OPERATION_SUCCESS:
                        # For each group, get all users
                        users_in_group = get_all_users(group_id, logger)
                    for user in users_in_group:
                        user_uuid = user[tenant_settings.USER_UUID_EXCHANGE_KEY]
                        users_array.append(user_uuid)
                    if tenant_settings.IS_OPERATION_SUCCESS and len(users_array) != 0:
                        # Remove all users from the group
                        rem_users_resp = remove_users_from_group(iam_customer_org_uuid, group_id, users_array, logger)
                    if tenant_settings.IS_OPERATION_SUCCESS and (group_id is not None):
                        # delete the group
                        delete_idm_group(iam_customer_org_uuid, group_id, logger)
                if tenant_settings.IS_OPERATION_SUCCESS:
                    # get all users in an org
                    all_users_parent_org = get_all_users_in_org(iam_customer_org_uuid)
                    for user_in_org in all_users_parent_org:
                        user_uuid = user_in_org[tenant_settings.USER_UUID_EXCHANGE_KEY]
                        # delete user from forgerock
                        delete_fr_user(user_uuid)
                if tenant_settings.IS_OPERATION_SUCCESS:
                    # delete the org
                    delete_fr_org(iam_customer_org_uuid)
                if not tenant_settings.IS_OPERATION_SUCCESS:
                    logger.warning('Wipe-Out Failed - Retry Again.')
                logger.info('DataServices IAM Customer Data Wipe-Out - Finished.')
                logger.info('Restore corresponding customer IAM Data from generated IAM User Dump file')
        else:
            logger.info('Exiting !!!')
    else:
        logger.warning('Not a proper operation. Exiting !!!')
