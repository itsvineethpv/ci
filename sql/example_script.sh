#!/bin/bash
set -e

SERVER=''
USER=''
PASSWD=''
FILE=''
MYSQL=$(which mysql)

usage() {
  cat << EOF
usage: $0 options

This script will apply database schema to the target DB server

OPTIONS:
  -h      Show this message
  -s      Server address
  -u      DB username
  -p      DB password
  -f      SQL file to execute
  -v      Verbose
EOF
}

failure() {
  MSG="SQL updates failed with: $BASH_COMMAND"
  echo $MSG
  exit 1
}

# Trap any error and call failure() before exit.
trap 'failure' EXIT

# Parse CLI args.
while getopts “hs:u:p:f” OPTION; do
    case $OPTION in
        h)
            usage
            exit 1
            ;;
        s)
            SERVER=$OPTARG
            ;;
        u)
            USER=$OPTARG
            ;;
        p)
            PASSWD=$OPTARG
            ;;
        f)
            FILE=$OPTARG
            ;;
        ?)
            usage
            exit
            ;;
    esac
done

# Determine if all required args have been passed in, if not display help.
if [[ -z $USER ]] || [[ -z $SERVER ]] || [[ -z $PASSWD ]] || [[ -z $FILE ]]; then
    usage
    exit 1
fi

echo "Applying schema file ${FILE} to server ${SERVER}"
$MYSQL -u $USER -h $SERVER -p "$PASSWD" < "$FILE"

trap - EXIT
