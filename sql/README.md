HSDP Deployment template for Cloud Foundry : sql directory
==========================================================

This directory is optional and should only be used when your application
cannot manage its own schema. If this is the case, use this directory to
hand off all SQL files to operations.

Application developers are responsible for providing all files needed
to execute the updates, along with instructions for how to install them,
specified in a detailed README file.

You can assume your audience knows how to connect to the DB in your
environment (for example, you do not need to provide instructions on
how to use SSH or set up port forwarding), however you should provide any
instructions operators might need to obtain DB credentials (depending on the
deployment, you may be able to create a service key, or the `get-credentials`
tool can help. See the next section for more information).


Extracting DB credentials
--------------------------

Depending on your use case this may or may not be required.  If you need
to connect to the DB from outside of Cloud Foundry you will need to do
one of two things: either create a service key for the service, or extract
the credentials from the running service.  Service keys are the preferred
way to do this, but they are only available if the service broker that
created the service has implemented the ability to provision service keys.
Refer to the broker documentation for your use case to determine if this
is an option.  Otherwise you can use the `get-credentials` tool in the
root of this repo to extract the master credentials for the service.

Example usage for the `get-credentials` tool:

    Usage:
      get-credentials [-h] -c CONFIG -s SERVICE_NAME

          -c : provide config file in configurations folder
          -s : service instance name from which credentials params to be
               extracted

    Example:
        get-credentials -c ./configurations/ph_deploy_config.yml -s static_db


Executing SQL files
--------------------

In the trivial case, you may be able to simply document the complete
sequence of `psql` or `mysql` commands needed to apply SQL changes (as
shown in the example below). In more complex cases, scripts may be provided
to apply schema changes, however they should be written in such a way as
to be reusable across multiple environments. Your script should accept
named command line options to specify environment or deployment-specific
parameters rather than reading variables from the shell environment, as
this can be more error-prone.  Refer to the script `example_script.sh`
in this directory and adapt it to your needs.

    Example:
      mysql -h <host> --port <port> -p <password> -u <user> < <filename>

