#!/usr/bin/python

# Common Headers for forgerock calls
FR_HEADER_ACCEPT = "Accept"
FR_HEADER_OPENIDM_USERNAME = "X-OpenIDM-Username"
FR_HEADER_OPENIDM_PASSWORD = "X-OpenIDM-Password"

HTTP_CONTENT_TYPE_JSON = "application/json"
HEADER_KEY_CONTENT_TYPE = "Content-Type"

# User types
USER_TYPE_HEALTH_CARE_ADMIN = "Healthcare Admin"
USER_TYPE_PRACTITIONER = "Practitioner"

# HTTP Verbs
HTTP_VERB_GET = "GET"
HTTP_VERB_POST = "POST"
HTTP_VERB_PUT = "PUT"
HTTP_VERB_DELETE = "DELETE"

# Property file keys
KEY_HOST = "host"
KEY_ADMIN_USER = "admin.user"
KEY_ADMIN_PASS = "admin.password"
KEY_ORGUUID = "orgUUID"
KEY_DB_PORT = "port"
KEY_DB_NAME = "dbname"
KEY_DB_USERNAME = "username"
KEY_DB_PASSWORD = "password"
KEY_IDM_HMAC_ID = "hmac.id"
KEY_IDM_HMAC_SECRET = "hmac.secret"
KEY_IDM_HOST = "idm.host"
KEY_IAM_HOST = "iam.host"
KEY_IDM_ENTERPRISE_USER_LOGIN = "enterprise.user.login"
KEY_IDM_ENTERPRISE_USER_PASSWORD = "enterprise.user.password"
KEY_IDM_PARENT_ORG_NAME = "parent.org.name"
KEY_IAM_OAUTH_ID = "oauth.client.id"
KEY_IAM_OAUTH_SECRET = "oauth.client.secret"
KEY_CDR_HCADMIN_USER_ID = "hcadmin.user.login"
KEY_CDR_HCADMIN_USER_PASSWORD = "hcadmin.user.password"
KEY_CDR_PHR_ADMIN_USER_ID = "admin.user.login"
KEY_CDR_PHR_ADMIN_USER_PASSWORD = "admin.user.password"
KEY_CDR_ORG_IDENTIFIER_SYSTEM = "fhir.org.identifier.system"
KEY_CDR_FULL_ORG_NAME = "fully.qualified.org.name"
KEY_ONBOARD_TENANT_ORG_NAME = "new.tenant.org.name"
KEY_ONBOARD_SHAREABLE_GRP_NAMES = "shareable.group.names"
KEY_ONBOARD_DATASHARE_HOST = "datashare.server.host"
KEY_ONBOARD_CUST_ORG_NAME = "customer.org.name"
KEY_ONBOARD_HCADMIN_LOGIN = "hcadmin.login.id"
KEY_ONBOARD_PROGRAM_NAME = "program.name"
KEY_ONBOARD_EMR_ACC_LOGIN = "emr.svc.acc.login.id"
KEY_ONBOARD_IHE_ACC_LOGIN = "ihe.svc.acc.login.id"
KEY_ONBOARD_JOBMGMT_ACC_LOGIN = "jobmgmt.svc.acc.login.id"
KEY_ONBOARD_HSDP_SECURITY_PATH = "hsdp.security.pkg.scripts.rel.path"
KEY_ONBOARD_CONSUMER_ADMIN_LOGIN = "consumer.org.admin.login.id"
KEY_ONBOARD_DEVICEDATA_ACC_LOGIN = "device.data.acc.login.id"
KEY_ONBOARD_HSDPAPP_ORG_NAME = "hsdp.app.svcs.org.name"
KEY_ONBOARD_CONSUMER_ORG_NAME = "consumer.phr.org.name"
KEY_ONBOARD_HSDP_APP_ORG_LOGIN = "hsdp.app.svcs.admin.login.id"
KEY_SHARE_ORG_UPDATE = "share.org.update"
# Dictionary Keys
GROUP_NAME_KEY = 'GROUP_NAME'
GROUP_ID_KEY = 'GROUP_ID'
USER_UUID_EXCHANGE_KEY = 'userUUID'

IS_OPERATION_SUCCESS = False
idm_host = ''
iam_host = ''
idm_hmac_id = ''
idm_hmac_secret = ''
idm_enterprise_user_login = ''
idm_enterprise_user_password = ''
oauth_client_id = ''
oauth_client_secret = ''
ehr_hcadmin_user_id = ''
ehr_hcadmin_user_password = ''
phr_admin_user_id = ''
phr_admin_user_password = ''
dbname = ''
dbhost = ''
dbport = ''
dbusername = ''
dbpassword = ''
fhir_admin_user_id = ''
fhir_admin_user_password = ''
customer_org_name = ''
new_tenant_org_name = ''
migrate_tenant_org_name = ''
hcadmin_login_to_assign = ''
hsdp_security_pkg_rel_path = ''
hsdp_app_svcs_org_name = ''
hsdp_app_svcs_login_id = ''
emr_svc_acc_login_id = ''
ihe_svc_acc_login_id = ''
jobmgmt_svc_acc_login_id = ''
consumer_org_admin_login_id = ''
consumer_phr_org_name = ''
fhir_org_identifier_system = ''
cdr_fhir_host_ehr = ''
cdr_fhir_host_phr = ''
parent_org_name = ''
datashare_host = ''
isshareupdate = ''
forgerock_adminuser=''
forgerock_host=''
forgerock_adminpass=''
devicedata_acc_login_id = ''
full_qualified_org_name = ''
output_dir_path_rel = 'output/'
HCADMIN_GROUP_NAME = "HCAdmin"
PRACTITIONER_GROUP_NAME = "Practitioners"
IHE_SVC_GROUP_NAME = "IHEServices"
IHE_ADMIN_GROUP_NAME = "IHEAdminGroup"
EMR_DATA_SVC_GROUP_NAME = "EMRDataServices"
JOB_MANAGEMENT_SVC_GROUP_NAME = "JobManagementSvcs"
standard_group_names = [HCADMIN_GROUP_NAME, PRACTITIONER_GROUP_NAME, IHE_SVC_GROUP_NAME,
                        IHE_ADMIN_GROUP_NAME, EMR_DATA_SVC_GROUP_NAME, JOB_MANAGEMENT_SVC_GROUP_NAME]
HSDP_APP_SVCS_ORG_NAME = "HSDPApplicationSvcs"

CONSUMER_ORG_NAME = "Consumer"
DEVICE_DATA_GRP_NAME = "DeviceDataServices"
PHILIPS_ADMIN_GRP_NAME = "PhilipsAdmin"
VALIDIC_GRP_NAME = "validicgroup"
phr_group_names = [DEVICE_DATA_GRP_NAME, PHILIPS_ADMIN_GRP_NAME, VALIDIC_GRP_NAME]

FHIR_ID_SYSTEM = 'https://identity.philips-healthsuite.com/organization'

# YAML Generation related Variable
orgs = {}
all_org = []
customer_org = {}
customer_user_ar = []
tenant_sub_org = {}
tenant_sub_group = {}
tenant_sub_group_ar = []
hsdp_org = {}
hsdp_users = []
consumer_org = {}
customer_users = {}
consumer_user_ar = []
consumer_groups = []
cosumer_group_user = []