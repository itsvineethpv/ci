#!/usr/bin/python
# Name: onboard_tenant.py
# Author: Amit Kumar, amit.k@philips.com
# Edited By: Swarit Agarwal
# Description: This script onboards a tenant in a customer organization in Dataservices R1.8 IAM setup
# Created: 18th October 2016
# Modified: 25th October 2016
# #
# Copyright Philips India, 2016
#

import subprocess
from lib.common_calls import *


logger = ''
cdr_fhir_host = ''
cust_org_name = ''
new_tenant_org_name = ''
hcadmin_login_to_assign = ''
hsdp_security_pkg_rel_path = ''
hsdp_app_svcs_org_name = ''
hsdp_app_svcs_login_id = ''
emr_svc_acc_login_id = ''
ihe_svc_acc_login_id = ''
jobmgmt_svc_acc_login_id = ''
consumer_org_admin_login_id = ''
consumer_phr_org_name = ''
devicedata_acc_login_id = ''
fhir_org_identifier_system = ''
start_number_of_org = 0
organization_structure = {}


def generate_fhir_org_resource(org_name, org_identifier_system, org_identifier_value):
    logger.debug('Generate FHIR Org resource json - Operation Started')
    fhir_org_resource = {
        'resourceType': 'Organization',
        'identifier': [
            {
                'use': 'usual',
                'system': org_identifier_system,
                'value': org_identifier_value
            }
        ],
        'name': org_name,
        'active': 'true'
    }
    logger.debug('Finished generating org resource payload : '
                % fhir_org_resource);
    return fhir_org_resource


def create_fhir_org(org_resource_payload):
    logger.debug('Generate FHIR Org resource json - Operation Started')
    create_org_url = 'https://' + cdr_fhir_host + '/cdr/api/Organization'
    create_org_header = get_cdr_fhir_header(logger)
    create_org_resp = rest_call(url=create_org_url , verb=tenant_settings.HTTP_VERB_POST ,
                                headers_param=create_org_header , body=org_resource_payload)
    logger.debug('Create Fhir Organization - Operation Finished')


def create_program_org_admin_user(program_name, org_name, admin_email, hsdp_security_pkg_relative_path, file_path):
    try:
        curr_dir = os.getcwd()
        curr_dir += hsdp_security_pkg_relative_path
        command = 'python hsdp_onboard.py -p ' + program_name + ' -o ' + org_name + ' -e ' + admin_email
        process_local = subprocess.Popen(command, stdout=subprocess.PIPE, shell=True, cwd=curr_dir)
        output_local = process_local.communicate()
        to_write = output_local[0]
        with open(file_path, 'a') as ofile:
            ofile.write(to_write)
        print to_write
        logger.info(to_write)
        return to_write
    except:
        logger.error('Org creation failed %s', org_name)


def parse_output_get_orgid(output):
    logger.info('Parsing the hsdp_onboard script logs and getting the orgId')
    org_id = ''
    if output is not None:
        for row in output.split('\n'):
            if 'Organization created successfully, Org Name:' in row:
                arr = row.split(': ')
                if len(arr) == 3:
                    org_id = arr[2]
                    org_id = org_id.strip()
                    logger.info('Extracted orgId from the script logs : %s', org_id)
            elif 'Creating user for the Organization, loginId:' in row:
                arr = row.split(': ')
                if len(arr) == 3:
                    user_password = arr[2].strip()
                    logger.info('User Password been extracted from response')
    return {'org_id': org_id, 'user_password': user_password}


def load_variable():
    global cust_org_name
    global new_tenant_org_name
    global hcadmin_login_to_assign
    global hsdp_security_pkg_rel_path
    global hsdp_app_svcs_org_name
    global hsdp_app_svcs_login_id
    global emr_svc_acc_login_id
    global ihe_svc_acc_login_id
    global jobmgmt_svc_acc_login_id
    global consumer_org_admin_login_id
    global consumer_phr_org_name
    global devicedata_acc_login_id
    global fhir_org_identifier_system
    global cdr_fhir_host
    cust_org_name = tenant_settings.customer_org_name
    new_tenant_org_name=tenant_settings.new_tenant_org_name
    hcadmin_login_to_assign=tenant_settings.hcadmin_login_to_assign
    hsdp_security_pkg_rel_path=tenant_settings.hsdp_security_pkg_rel_path
    hsdp_app_svcs_org_name = tenant_settings.hsdp_app_svcs_org_name
    emr_svc_acc_login_id = tenant_settings.emr_svc_acc_login_id
    ihe_svc_acc_login_id = tenant_settings.ihe_svc_acc_login_id
    jobmgmt_svc_acc_login_id = tenant_settings.jobmgmt_svc_acc_login_id
    consumer_org_admin_login_id = tenant_settings.consumer_org_admin_login_id
    consumer_phr_org_name = tenant_settings.consumer_phr_org_name
    devicedata_acc_login_id = tenant_settings.devicedata_acc_login_id
    fhir_org_identifier_system = tenant_settings.fhir_org_identifier_system
    cdr_fhir_host = tenant_settings.cdr_fhir_host_ehr
    hsdp_app_svcs_login_id = tenant_settings.hsdp_app_svcs_login_id


def onboard_tenant(parent_org_option , tenant_type , log):
    global logger
    logger = log
    load_variable()
    logger.info('DataServices IAM Customer/Tenant Onboarding - Operation Started.')
    iam_customer_org_uuid = ''
    new_tenant_org_uuid = ''
    hsdp_app_svcs_org_id = ''
    fhir_org_name = ''
    org_idm_uuid = ''
    hcadmin_password= ''
    hsdp_app_svcs_password=''
    get_org_response = get_org_by_name(cust_org_name, logger)
    tenant_type = tenant_type.lower()
    if tenant_type == 'ehr':
        if parent_org_option.lower() == 'new':
            logger.info('Onboarding a new customer :  %s  and a new tenant : %s', cust_org_name, new_tenant_org_name)
            # create a filename for the hsdp_onboard script logs dump
            current_timestamp = datetime.now().strftime('%Y-%m-%d %H-%M-%S')
            onboard_output_file = tenant_settings.output_dir_path_rel + cust_org_name +\
                                  '_' + current_timestamp.replace(' ', '_') + '.txt'
            if (get_org_response is not None) and ('responseCode' in get_org_response):
                response_code = get_org_response['responseCode']
                if response_code == '4006':
                    output = create_program_org_admin_user(cust_org_name, cust_org_name, hcadmin_login_to_assign,
                                                           hsdp_security_pkg_rel_path, onboard_output_file)
                    iam_customer_org_response = parse_output_get_orgid(output)
                    iam_customer_org_uuid = iam_customer_org_response['org_id']
                    hcadmin_password = iam_customer_org_response['user_password']
                    if iam_customer_org_uuid != '':
                        log_msg = 'Successfully created the customer org name : ' + cust_org_name, ' id : ' +\
                            iam_customer_org_uuid
                        logger.info(log_msg)
                        log_msg = 'Get the HMAC keys, OAuth id/secret, OrgAdmin credentials for Customer' \
                                  ' org from this file -> ' + onboard_output_file
                        logger.info(log_msg)
                        tenant_settings.customer_org = dict(org_name=cust_org_name, org_uuid=iam_customer_org_uuid)
                    else:
                        log_msg = 'Customer org creation failed : ' + cust_org_name
                elif response_code == '200':
                    logger.info('Customer Organization %s is already exists', cust_org_name)
            # verify and create the hsdp application services organization
            get_hsdp_app_org_resp = get_org_by_name(hsdp_app_svcs_org_name, logger)
            if (get_hsdp_app_org_resp is not None) and ('responseCode' in get_hsdp_app_org_resp):
                response_code = get_hsdp_app_org_resp['responseCode']
                if response_code == '4006':
                    current_timestamp = datetime.now().strftime('%Y-%m-%d %H-%M-%S')
                    hsdp_app_output_file = tenant_settings.output_dir_path_rel + hsdp_app_svcs_org_name +\
                                           '_' + current_timestamp.replace(' ', '_') + '.txt'
                    hsdp_app_org_output = create_program_org_admin_user(hsdp_app_svcs_org_name,
                                                                        hsdp_app_svcs_org_name,
                                                                        hsdp_app_svcs_login_id,
                                                                        hsdp_security_pkg_rel_path,
                                                                        hsdp_app_output_file)
                    hsdp_app_svcs_org_response = parse_output_get_orgid(hsdp_app_org_output)
                    hsdp_app_svcs_org_id = hsdp_app_svcs_org_response['org_id']
                    hsdp_app_svcs_password = hsdp_app_svcs_org_response['user_password']
                    if hsdp_app_svcs_org_id != '':
                        log_msg = 'Successfully created the hsdp app svcs org : ' + hsdp_app_svcs_org_name +\
                            ' with id : ' + hsdp_app_svcs_org_id
                        logger.info(log_msg)
                        log_msg = 'Get the HMAC keys, OAuth id/secret, OrgAdmin credentials for ' \
                                  'HSDP App Svcs org from this file -> ' + hsdp_app_output_file
                        logger.info(log_msg)
                    else:
                        log_msg = 'Failed to create the hsdp app svcs org : ' + hsdp_app_svcs_org_name
                        logger.warning(log_msg)
                elif response_code == '200':
                    hsdp_app_svcs_org_id = get_uuid_from_org_response(get_hsdp_app_org_resp, logger)
                    logger.info('HSDP Organization %s already exists. OrgID : %s',hsdp_app_svcs_org_name, hsdp_app_svcs_org_id)
                tenant_settings.hsdp_org = dict(org_name=hsdp_app_svcs_org_name, org_uuid=hsdp_app_svcs_org_id)
        if parent_org_option.lower() == 'existing' or parent_org_option.lower() == 'new':
            if (iam_customer_org_uuid is None) or (iam_customer_org_uuid == ''):
                iam_customer_org_uuid = get_uuid_from_org_response(get_org_response, logger)
                if iam_customer_org_uuid != '':
                    logger.info('Customer org details ::: OrgName : %s ID : %s',cust_org_name, iam_customer_org_uuid)
                    if cust_org_name and iam_customer_org_uuid:
                        tenant_settings.customer_org = dict(org_name=cust_org_name, org_uuid=iam_customer_org_uuid)
                else:
                    logger.warning('Organization not found. OrgName : %s', cust_org_name)

            if tenant_settings.IS_OPERATION_SUCCESS and (iam_customer_org_uuid != ''):
                # create tenant org inside the parent org
                create_child_resp = create_child_org(iam_customer_org_uuid, new_tenant_org_name, logger)
                if 'exchange' in create_child_resp:
                    new_tenant_org_uuid = create_child_resp['exchange']['organizationId']
                    if new_tenant_org_uuid != '':
                        logger.info('Tenant Org Name : %s, Tenant Org ID : %s and Parent Org ID : %s',
                                    new_tenant_org_name, new_tenant_org_uuid, iam_customer_org_uuid)
                        tenant_settings.tenant_sub_org = dict(sub_org_name=new_tenant_org_name,
                                                              sub_org_uuid=new_tenant_org_uuid)
                        # We store the new tenant org UUID in an external file so that any <env>_input_to_yaml.yml file can use it
			new_tenant_org_uuid_file = open("/home/new-tenant-org-uuid.txt", "w")
			logger.debug('Exporting the tenant org UUID for later use (' + new_tenant_org_uuid + ')')			
			new_tenant_org_uuid_file.write(new_tenant_org_uuid)
			new_tenant_org_uuid_file.close()
                if tenant_settings.IS_OPERATION_SUCCESS:
                    for group_name in tenant_settings.standard_group_names:
                        # create same group in tenant org
                        new_group_id = ''
                        create_grp_resp = create_idm_group(new_tenant_org_uuid, group_name, logger)
                        if 'exchange' in create_grp_resp:
                            new_group_id = create_grp_resp['exchange']['groupID']
                            logger.info('Group Name : %s Group Id : %s and Org Id : %s',
                                        group_name, new_group_id, new_tenant_org_uuid)
                            group_detail = dict(group_name=group_name, group_uuid=new_group_id)

                        if tenant_settings.IS_OPERATION_SUCCESS and (new_group_id is not None):
                            if group_name == 'HCAdmin':
                                # Get the hcadmin user
                                hcadmin_uuid = ''
                                get_user_resp = get_user_by_loginid(hcadmin_login_to_assign, logger)
                                if (get_user_resp is not None) and ('exchange' in get_user_resp):
                                    hcadmin_uuid = get_user_resp['exchange']['users'][0]['userUUID']
                                if tenant_settings.IS_OPERATION_SUCCESS and (hcadmin_uuid != ''):
                                    logger.info('HCAdmin user found with LoginId : %s and User ID : %s'
                                                , hcadmin_login_to_assign, hcadmin_uuid)
                                    assign_user_to_group([hcadmin_uuid], new_group_id, logger)
                                    if tenant_settings.IS_OPERATION_SUCCESS:
                                        logger.info('Assigned User to Group Successfully:: login Id : %s(UUID : %s) to '
                                                    'Group Id : %s',hcadmin_login_to_assign, hcadmin_uuid, new_group_id)
                                        hcadmin = dict(login_id= hcadmin_login_to_assign, user_uuid = hcadmin_uuid)
                                        tenant_settings.customer_user_ar.append(hcadmin)

                                else:
                                    log_msg = 'HCAdmin User not found. LoginId : ' + hcadmin_login_to_assign
                                    logger.warning(log_msg)
                                if hcadmin_login_to_assign.strip() != '':
                                    hcadmin_svc = dict(login_id=hcadmin_login_to_assign)
                                if hcadmin_uuid.strip() != '':
                                    hcadmin_svc.setdefault('user_uuid', hcadmin_uuid)
                                if hcadmin_password != '':
                                    hcadmin_svc.setdefault('password', hcadmin_password)
                                group_detail.setdefault('user', hcadmin_svc)
                            elif parent_org_option.lower() == 'new':
                                if hsdp_app_svcs_org_id != '' and group_name == tenant_settings.EMR_DATA_SVC_GROUP_NAME:
                                    # create the emr user and assign to emr group
                                    create_user_resp = create_idm_user(emr_svc_acc_login_id, hsdp_app_svcs_org_id,
                                                                       'EMR User', 'EMR User', 'EMR User', logger)

                                    emr_svc_acc_user_uuid = get_user_uuid_4m_response(create_user_resp['response'], logger)
                                    if emr_svc_acc_user_uuid != '':
                                        assign_user_to_group([emr_svc_acc_user_uuid], new_group_id, logger)
                                        if tenant_settings.IS_OPERATION_SUCCESS:
                                            log_msg = 'Assigned User to Group :: LoginId : ' + emr_svc_acc_login_id + \
                                                      ' GrpId : ' + new_group_id
                                            logger.info(log_msg)
                                    if emr_svc_acc_login_id.strip() != '':
                                        emr_svc = dict(login_id=emr_svc_acc_login_id)
                                        tenant_settings.hsdp_users.append(emr_svc_acc_login_id)
                                    if emr_svc_acc_user_uuid.strip() != '':
                                        emr_svc.setdefault('user_uuid', emr_svc_acc_user_uuid)
                                    if create_user_resp['password'] != '':
                                        emr_svc.setdefault('password', create_user_resp['password'])
                                    group_detail.setdefault('user', emr_svc)
                                if hsdp_app_svcs_org_id != '' and group_name == tenant_settings.IHE_SVC_GROUP_NAME\
                                        and tenant_settings.IS_OPERATION_SUCCESS:
                                    # create the ihe user and assign to ihe group
                                    create_user_resp = create_idm_user(ihe_svc_acc_login_id, hsdp_app_svcs_org_id,
                                                                       'IHE Svc User', 'IHE Svc User', 'IHE Svc User',
                                                                       logger)
                                    ihe_svc_acc_user_uuid = get_user_uuid_4m_response(create_user_resp['response'], logger)
                                    if ihe_svc_acc_user_uuid != '':
                                        assign_user_to_group([ihe_svc_acc_user_uuid], new_group_id, logger)
                                        if tenant_settings.IS_OPERATION_SUCCESS:
                                            log_msg = 'Assigned User to Group :: LoginId : ' + ihe_svc_acc_login_id + \
                                                      ' GrpId : ' + new_group_id
                                            logger.info(log_msg)
                                    if ihe_svc_acc_login_id.strip() != '':
                                        ihe_svc = dict(login_id=ihe_svc_acc_login_id)
                                        tenant_settings.hsdp_users.append(ihe_svc_acc_login_id)
                                    if ihe_svc_acc_user_uuid.strip() != '':
                                        ihe_svc.setdefault('user_uuid', ihe_svc_acc_user_uuid)
                                    if create_user_resp['password'] != '':
                                        ihe_svc.setdefault('password', create_user_resp['password'])
                                    group_detail.setdefault('user', ihe_svc)
                                if hsdp_app_svcs_org_id != '' and group_name == tenant_settings.\
                                        JOB_MANAGEMENT_SVC_GROUP_NAME and tenant_settings.IS_OPERATION_SUCCESS:
                                    # create the job user and assign to job group
                                    create_user_resp = create_idm_user(jobmgmt_svc_acc_login_id, hsdp_app_svcs_org_id,
                                                                       'JobService User', 'JobService User',
                                                                       'JobService User', logger)
                                    jobmgmt_svc_acc_user_uuid = get_user_uuid_4m_response(create_user_resp['response'], logger)
                                    if jobmgmt_svc_acc_user_uuid != '':
                                        assign_user_to_group([jobmgmt_svc_acc_user_uuid], new_group_id, logger)
                                        if tenant_settings.IS_OPERATION_SUCCESS:
                                            log_msg = 'Assigned User to Group :: LoginId : ' + jobmgmt_svc_acc_login_id + \
                                                      ' GrpId : ' + new_group_id
                                            logger.info(log_msg)
                                    if jobmgmt_svc_acc_login_id.strip() != '':
                                        job_svc = dict(login_id=jobmgmt_svc_acc_login_id)
                                        tenant_settings.hsdp_users.append(jobmgmt_svc_acc_login_id)
                                    if jobmgmt_svc_acc_user_uuid.strip() != '':
                                        job_svc.setdefault('user_uuid', jobmgmt_svc_acc_user_uuid)
                                    if create_user_resp['password'] != '':
                                        job_svc.setdefault('password', create_user_resp['password'])
                                    group_detail.setdefault('user', job_svc)
                        tenant_settings.tenant_sub_group_ar.append(group_detail)
            if tenant_settings.IS_OPERATION_SUCCESS:
                if (new_tenant_org_uuid != '') and (new_tenant_org_name != ''):
                    fhir_org_name = cust_org_name + ":" + new_tenant_org_name
                    org_idm_uuid = new_tenant_org_uuid

                    if len(tenant_settings.tenant_sub_group_ar) != 0:
                        tenant_settings.tenant_sub_org.setdefault('groups', tenant_settings.tenant_sub_group_ar)
                    if len(tenant_settings.customer_user_ar) != 0:
                        tenant_settings.customer_org.setdefault('users', tenant_settings.customer_user_ar)
                    if len(tenant_settings.tenant_sub_org) != 0:
                        tenant_settings.customer_org.setdefault('sub_org', tenant_settings.tenant_sub_org)
                    if len(tenant_settings.hsdp_users) != 0:
                        tenant_settings.hsdp_org.setdefault('users', tenant_settings.hsdp_users)
                    if len(tenant_settings.customer_org) != 0:
                        tenant_settings.all_org.append(tenant_settings.customer_org)
                    if len(tenant_settings.hsdp_org) != 0:
                        tenant_settings.all_org.append(tenant_settings.hsdp_org)
                    orgs = dict(orgs=tenant_settings.all_org)
                    current_timestamp = datetime.now().strftime('%Y-%m-%d %H-%M-%S')
                    file_name = parent_org_option.lower()+'_ehr_organization_'+current_timestamp+'.yaml'
                    logger.info('YAML file named %s is getting generated', file_name)
                    generate_yaml(file_name, orgs)
                else:
                    logger.warning('Tenant org id or tenant org name is not available.')

    elif tenant_type == 'phr' and consumer_org_admin_login_id != '':
        consumer_org_uuid = ''
        consumer_admin_uuid = ''
        parent_org_option = 'new'
        logger.info('Starting PHR tenant Onboarding. org-name : %s', consumer_phr_org_name)
        get_consumer_org_resp = get_org_by_name(consumer_phr_org_name, logger)
        if (get_consumer_org_resp is not None) and ('responseCode' in get_consumer_org_resp):
            response_code = get_consumer_org_resp['responseCode']
            if response_code == '4006':
                current_timestamp = datetime.now().strftime('%Y-%m-%d %H-%M-%S')
                consumer_output_file = tenant_settings.output_dir_path_rel + consumer_phr_org_name +\
                                       '_' + current_timestamp.replace(' ', '_') + '.txt'
                output = create_program_org_admin_user(consumer_phr_org_name, consumer_phr_org_name,
                                                       consumer_org_admin_login_id, hsdp_security_pkg_rel_path,
                                                       consumer_output_file)
                consumer_org_response = parse_output_get_orgid(output)
                consumer_org_uuid = consumer_org_response['org_id']
                consumer_admin_password = consumer_org_response['user_password']
                if consumer_org_uuid != '':
                    log_msg = 'Successfully created the Consumer org with id : ' + consumer_org_uuid
                    logger.info(log_msg)
                    log_msg = 'Get the HMAC keys, OAuth id/secret, OrgAdmin credentials for ' \
                              'Consumer Org from this file -> ' + consumer_output_file
                    logger.info(log_msg)
            else:
                logger.info('Consumer organization already exists. Getting the organization uuid.')
                consumer_org_uuid = get_uuid_from_org_response(get_consumer_org_resp, logger)
                log_msg = 'Fetched the existing Consumer org uuid : ' + consumer_org_uuid
                logger.info(log_msg)
        tenant_settings.consumer_org = dict(org_name=consumer_phr_org_name, org_uuid=consumer_org_uuid)
        if consumer_org_uuid != '':
            for phr_grp_name in tenant_settings.phr_group_names:
                create_grp_resp = create_idm_group(consumer_org_uuid, phr_grp_name, logger)
                if 'exchange' in create_grp_resp:
                    new_group_id = create_grp_resp['exchange']['groupID']
                    log_msg = 'Group created :: Name : ' + phr_grp_name + ' Id : ' + new_group_id + \
                              ' OrgId : ' + consumer_org_uuid
                    logger.info(log_msg)
                    group_detail = dict(group_name=phr_grp_name, group_uuid=new_group_id)
                if tenant_settings.IS_OPERATION_SUCCESS and new_group_id != '':
                    if phr_grp_name == tenant_settings.PHILIPS_ADMIN_GRP_NAME:
                        get_user_resp = get_user_by_loginid(consumer_org_admin_login_id, logger)
                        if (get_user_resp is not None) and ('exchange' in get_user_resp):
                            consumer_admin_uuid = get_user_resp['exchange']['users'][0]['userUUID']
                        if tenant_settings.IS_OPERATION_SUCCESS and (consumer_admin_uuid is not None):
                            assign_user_to_group([consumer_admin_uuid], new_group_id, logger)
                            if tenant_settings.IS_OPERATION_SUCCESS:
                                log_msg = 'Assigned User to Group :: LoginId : ' + consumer_org_admin_login_id + \
                                          ' GrpId : ' + new_group_id
                                logger.info(log_msg)
                                consumer_admin = dict(login_id=consumer_org_admin_login_id, uuid=consumer_admin_uuid)
                                tenant_settings.consumer_user_ar.append(consumer_admin)
                                if consumer_org_admin_login_id.strip() != '':
                                    con_adm = dict(login_id=consumer_org_admin_login_id)
                                if consumer_admin_uuid.strip() != '':
                                    con_adm.setdefault('user_uuid', consumer_admin_uuid)
                                if consumer_admin_password != '':
                                    con_adm.setdefault('password',consumer_admin_password)
                                group_detail.setdefault('user', con_adm)
                    elif phr_grp_name == tenant_settings.DEVICE_DATA_GRP_NAME:
                        # create devicedata user and assign to grp
                        create_user_resp = create_idm_user(devicedata_acc_login_id, consumer_org_uuid, 'DeviceData Acc',
                                                           'DeviceData Acc', 'DeviceData Acc', logger)
                        devicedata_acc_user_uuid = get_user_uuid_4m_response(create_user_resp['response'], logger)
                        if devicedata_acc_user_uuid != '':
                            assign_user_to_group([devicedata_acc_user_uuid], new_group_id, logger)
                            if tenant_settings.IS_OPERATION_SUCCESS:
                                log_msg = 'Assigned User to Group :: LoginId : ' + devicedata_acc_login_id + \
                                          ' GrpId : ' + new_group_id
                                logger.info(log_msg)
                                if devicedata_acc_login_id.strip() != '':
                                    dvcs_data = dict(login_id=devicedata_acc_login_id)
                                if devicedata_acc_user_uuid.strip() != '':
                                    dvcs_data.setdefault('user_uuid', devicedata_acc_user_uuid)
                                if create_user_resp['password'] != '':
                                    dvcs_data.setdefault('password', create_user_resp['password'])
                                group_detail.setdefault('user', dvcs_data)
                tenant_settings.consumer_groups.append(group_detail)
            if tenant_settings.IS_OPERATION_SUCCESS:
                fhir_org_name = consumer_phr_org_name
                org_idm_uuid = consumer_org_uuid    

                current_timestamp = datetime.now().strftime('%Y-%m-%d %H-%M-%S')

                if len(tenant_settings.consumer_groups) != 0:
                    tenant_settings.consumer_org.setdefault('groups', tenant_settings.consumer_groups)
                if len(tenant_settings.consumer_user_ar) != 0:
                    tenant_settings.consumer_org.setdefault('users', tenant_settings.consumer_user_ar)
                if len(tenant_settings.consumer_org) != 0:
                    tenant_settings.all_org.append(tenant_settings.consumer_org)
                orgs = dict(orgs=tenant_settings.all_org)
                file_name = 'phr_organization_' + current_timestamp + '.yaml'
                logger.info('YAML file named %s is getting generated', file_name)
                generate_yaml(file_name, orgs)
    else:
        logger.warning('Invalid Tenant Type entered. Possible values - EHR/PHR')
    # if tenant_settings.IS_OPERATION_SUCCESS and (fhir_org_name != '') and (org_idm_uuid != '')\
    #         and (parent_org_option.lower() == 'existing'):
    #     fhir_org_res = generate_fhir_org_resource(fhir_org_name, fhir_org_identifier_system,
    #                                               org_idm_uuid)
    #     create_fhir_org(fhir_org_res)
    #     logger.info('Finished creating an EHR tenant : ' + new_tenant_org_name  + 'for customer : ' + cust_org_name)
    if not tenant_settings.IS_OPERATION_SUCCESS:
        logger.warning('Onboarding Failed - Retry Again.')
    logger.info('DataServices IAM Customer/Tenant Onboarding - Operation Finished.')
