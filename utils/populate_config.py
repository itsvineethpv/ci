#!/usr/bin/env python
# pylint: disable=invalid-name
#
# This is disabled due to the use of single character variable names
# within context managers and list comprehensions.  Single character
# variables are acceptable in these use cases where the intent is
# clear within a couple of lines of code.
"""This script will populate config file for different environment taking
key/value information.
"""
from __future__ import print_function
import os
import argparse
import logging
import yaml
from jinja2 import Environment, FileSystemLoader

TEMPLATE_PATH = './'

logging.basicConfig(
    level=logging.DEBUG,
    format='%(asctime)s %(levelname)s %(message)s',
    filename='./deploy.log',
    filemode='a'
)

def log(msg):
    """Simple logging function.

    Simple function to print and log events during processing of manifest
    files.  The log file will be written to ./deploy.log.

    Args:
        msg (str): A log message.
    """
    print(msg)
    logging.debug(msg)

def detect_new_tenant_org_uuid():
    # Reading the new tenant org UUID if any
    
    try:
        with open('/home/new-tenant-org-uuid.txt', 'r') as f:
            new_tenant_org_uuid = f.readline()

        if(new_tenant_org_uuid == ''):
            raise ValueError('No tenant org UUID detected !')

        print('New tenant org uuid detected: ' + new_tenant_org_uuid)

        # Reading the key value yaml file
        with open('/home/new-tenant-org-uuid.txt', 'r') as f:
            newlines = []
            for line in f.readlines():
                if(line.strip().startswith('ehr_tenant_organization_uuid') or line.strip().startswith('phr_tenant_organization_uuid')):
                    newlines.append(line.split(":")[0] + ": " + new_tenant_org_uuid + "\n")
                else:
                    newlines.append(line)

        with open('/home/new-tenant-org-uuid.txt', 'w') as f:
            f.writelines(newlines)

        os.remove('/home/new-tenant-org-uuid.txt')

    except IOError as ioex:
    	print ('err message:' + os.strerror(ioex.errno))
        print("No new tenant org UUID detected. Going on execution ...")

def populate_config(config_file, config_keyvalue_file):
    """Renders all condif template files and populate with actual values.

    Args:
        config_file (str): The path to the yaml config file needed to populated.
        config_keyvalue_file (str): The path to the yaml key vlaue file.
    """

    #detect_new_tenant_org_uuid()

    with open(config_keyvalue_file) as _f:
        config_keyvalue_file = yaml.load(_f.read())
    head, cfgfilename = os.path.split(config_file)
    j2 = Environment(loader=FileSystemLoader(head), trim_blocks=True)
    template = j2.get_template(cfgfilename)
    config_contents = template.render(**config_keyvalue_file)
    config_name = '.'.join(cfgfilename.split('.')[:2])
    config_file = os.path.join(head, config_name)
    with open(config_file, 'w') as f:
        log('Generating {0}'.format(config_file))
        f.write(config_contents)

    log('Completed config generation.')


def parse_args():
    """Parse command line args.

    Simple function to parse and return command line args.

    Returns:
        argparse.Namespace: An argparse.Namespace object.
    """
    parser = argparse.ArgumentParser()
    #action = required_arg.add_mutually_exclusive_group(required=False)
    parser.add_argument('-p',
                        '--populate',
                        help='Create manifest files from a config file.')
    parser.add_argument('-i',
                        '--input',
                        help='Input file to populate the config file')
    args = parser.parse_args()
    return args


def main():
    """Main entry point.
    """
    args = parse_args()

    if args.populate and args.input:
        populate_config(args.populate, args.input)

if __name__ == '__main__':
    main()
