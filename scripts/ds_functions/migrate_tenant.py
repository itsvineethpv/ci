#!/usr/bin/python
# Name: migrate_tenant.py
# Author: Amit Kumar, amit.k@philips.com
# Edited By: Swarit Agarwal
# Description: This script migrates an existing DataServices R1.7 system IAM data to new R1.8 IAM setup
# Created: 18th October 2016
# Modified: 23rd October 2016
# #
# Copyright Philips India, 2016
#

from optparse import OptionParser
import os
import argparse
import logging
import logging.handlers

import yaml

from lib import tenant_settings
from lib.common_calls import *
from lib.create_hmac_signature import *

logger = ''
parent_org_name = ''
datashare_host = ''
new_tenant_org_name = ''
fhir_org_identifier_system = ''
isshareupdate = ''
cdr_fhir_host = ''


def get_fhir_org(identifier_system, identifier_value):
    logger.info('Fetch FHIR Organization for identifier : %s | %s',identifier_system, identifier_value)
    tenant_settings.IS_OPERATION_SUCCESS = False
    identifier_param = identifier_system + '|' + identifier_value
    get_fhir_org_url = 'https://' + cdr_fhir_host + '/cdr/api/Organization?identifier=' + identifier_param
    cdr_header = get_cdr_fhir_header(logger)
    get_org_response = rest_call(url=get_fhir_org_url, verb=tenant_settings.HTTP_VERB_GET,
                                 headers_param=cdr_header, body=None)
    org_resource = ''
    entry_key = 'entry'
    resource_key = 'resource'
    if (get_org_response is not None) and (entry_key in get_org_response):
        entry_arr = get_org_response[entry_key]
        if (entry_arr is not None) and len(entry_arr) != 0:
            entry = entry_arr[0]
            if (entry is not None) and (resource_key in entry):
                org_resource = entry[resource_key]
                tenant_settings.IS_OPERATION_SUCCESS = True
            else:
                logger.warning('resource element not found in entry of FHIR Organization response')
        else:
            logger.warning('entry array is empty in FHIR Org response')
    else:
        logger.warning('entry element not found in FHIR Organization response')
    logger.debug('Get FHIR Org - Operation Finished')
    return org_resource


def update_org_identifier(org_resource, identifier_system_old, identifier_value_old,
                          identifier_system_new, identifier_value_new, new_org_name):
    logger.debug('Updating Identifier for FHIR Org')
    identifier_key = 'identifier'
    identifier_system_key = 'system'
    identifier_value_key = 'value'
    if org_resource is not None:
        if identifier_key in org_resource:
            identifier_arr = org_resource[identifier_key]
            for id in identifier_arr:
                id_system = id[identifier_system_key]
                id_value = id[identifier_value_key]
                if (id_system == identifier_system_old) and (id_value == identifier_value_old):
                    id[identifier_system_key] = identifier_system_new
                    id[identifier_value_key] = identifier_value_new
                else:
                    logger.warning('Identifier not found : ', identifier_value_old)
        if 'name' in org_resource:
            org_resource['name'] = new_org_name
    logger.debug('Update Org Identifier - Operation Finished - ', org_resource)
    return org_resource


def update_fhir_org(org_resource):
    logger.info('Updating Organization on FHIR server %s', cdr_fhir_host)
    id_key = 'id'
    if (org_resource is not None) and (id_key in org_resource):
        fhir_logical_id = org_resource[id_key]
    update_org_url = 'https://' + cdr_fhir_host + '/cdr/api/Organization/' + fhir_logical_id
    cdr_header = get_cdr_fhir_header(logger)
    update_response = rest_call(url=update_org_url , verb=tenant_settings.HTTP_VERB_PUT ,
                                headers_param=cdr_header , body=org_resource)
    logger.debug('Update FHIR Org - Operation Finished')
    logger.info(json.dumps(update_response))
    return update_response


def update_share_database():
    connection = connect_db(logger)
    qualified_organization = parent_org_name+":"+new_tenant_org_name
    logger.info('replacing Organization %s with %s in data share.', parent_org_name, qualified_organization)
    cursor = connection.cursor(cursor_factory=psycopg2.extras.DictCursor)
    query = "Update data_share set organization_id= %s where organization_id = %s"
    data = (qualified_organization, parent_org_name)
    try:
        cursor.execute(query, data)
        logger.info("%s Organization has been updated", cursor.rowcount)
        connection.commit()
        tenant_settings.IS_OPERATION_SUCCESS= True
    except Exception as e:
        connection.rollback()
        logger.error(e)
    finally:
        cursor.close()
        connection.close()


def load_data():
    global parent_org_name
    global datashare_host
    global new_tenant_org_name
    global fhir_org_identifier_system
    global isshareupdate
    global cdr_fhir_host
    parent_org_name=tenant_settings.parent_org_name
    datashare_host = tenant_settings.datashare_host
    new_tenant_org_name = tenant_settings.migrate_tenant_org_name
    fhir_org_identifier_system = tenant_settings.fhir_org_identifier_system
    isshareupdate = tenant_settings.isshareupdate
    cdr_fhir_host = tenant_settings.cdr_fhir_host_ehr
    tenant_settings.fhir_admin_user_id = tenant_settings.ehr_hcadmin_user_id
    tenant_settings.fhir_admin_user_password = tenant_settings.ehr_hcadmin_user_password


def migrate_tenant(log):
    global logger
    logger = log
    load_data()
    logger.info('DataServices IAM Customer Data Migration R1.7 to R1.8 - Started.')
    shareable_group_names = 'DiabetesCareTeam,GeneralCareTeam'
    shareable_groups = shareable_group_names.split(',')
    iam_customer_org_uuid = ''
    new_tenant_org_uuid = ''
    get_org_resp = get_org_by_name(parent_org_name, logger)
    iam_customer_org_uuid = get_uuid_from_org_response(get_org_resp, logger)
    if tenant_settings.IS_OPERATION_SUCCESS and iam_customer_org_uuid != '':
        log_msg = 'Customer org fetched. OrgID : ' + iam_customer_org_uuid
        logger.info(log_msg)
        # create tenant org inside the parent org
        create_child_resp = create_child_org(iam_customer_org_uuid, new_tenant_org_name, logger)
        if 'exchange' in create_child_resp:
            new_tenant_org_uuid = create_child_resp['exchange']['organizationId']
    else:
        log_msg = 'Organisation with name : ' + parent_org_name + ' not found.'
        logger.warning(log_msg)
    if tenant_settings.IS_OPERATION_SUCCESS and new_tenant_org_uuid != '' and iam_customer_org_uuid != '':
        log_msg = 'Tenant sub-org with Id : ' + new_tenant_org_uuid + ' created inside customer orgId : '\
                  + iam_customer_org_uuid
        logger.info(log_msg)
        # get all groups in an org
        all_groups_parent_org = get_all_groups(iam_customer_org_uuid, logger)
        for group in all_groups_parent_org:
            users_array = []
            old_group_id = group[tenant_settings.GROUP_ID_KEY]
            group_name = group[tenant_settings.GROUP_NAME_KEY]
            # create same group in tenant org
            create_grp_resp = create_idm_group(new_tenant_org_uuid, group_name, logger)
            if 'exchange' in create_grp_resp:
                new_group_id = create_grp_resp['exchange']['groupID']
            if tenant_settings.IS_OPERATION_SUCCESS and (new_group_id is not None):
                log_msg = 'New group created. Name : ' + group_name + ' Id : ' + new_group_id +\
                          ' OrgId : ' + new_tenant_org_uuid
                logger.info(log_msg)
                # For each old group, get all users
                users_in_group = get_all_users(old_group_id, logger)
                for user in users_in_group:
                    user_uuid = user[tenant_settings.USER_UUID_EXCHANGE_KEY]
                    users_array.append(user_uuid)
            if tenant_settings.IS_OPERATION_SUCCESS and len(users_array) != 0:
                # Remove all users from the group
                remove_users_from_group(iam_customer_org_uuid, old_group_id, users_array, logger)
            if tenant_settings.IS_OPERATION_SUCCESS and len(users_array) != 0:
                # Add all users to the new group
                assign_user_to_group(users_array, new_group_id, logger)
            # create shareable group in datashare
            if tenant_settings.IS_OPERATION_SUCCESS and (shareable_groups is not None) and \
                    (group_name in shareable_groups):
                shareable_resp = create_shareable_group(datashare_host, group_name, new_group_id,
                                                        parent_org_name + ':' + new_tenant_org_name,
                                                        new_tenant_org_uuid, logger)
                outcome = check_create_shareable_grp_response(shareable_resp, logger)
                if outcome == 'created':
                    log_msg = 'Shareable group created successfully. Name : ' + group_name
                    logger.info(log_msg)
                    tenant_settings.IS_OPERATION_SUCCESS = True
                elif outcome == 'duplicate':
                    log_msg = 'Shareable group already exists. No need to create. Name : ' + group_name
                    logger.info(log_msg)
                    tenant_settings.IS_OPERATION_SUCCESS = True
                else:
                    log_msg = 'Shareable group issue : ' + outcome
                    logger.warning(log_msg)
                    tenant_settings.IS_OPERATION_SUCCESS = False
            if tenant_settings.IS_OPERATION_SUCCESS and (old_group_id is not None):
                # delete the old group
                delete_idm_group(iam_customer_org_uuid, old_group_id, logger)
        # update fhir org
        if new_tenant_org_uuid is not None:
            org_resource = get_fhir_org(fhir_org_identifier_system, iam_customer_org_uuid)
            if tenant_settings.IS_OPERATION_SUCCESS and (org_resource is not None) \
                    and (new_tenant_org_uuid != '') and (len(org_resource) > 0):
                org_resource = update_org_identifier(org_resource, fhir_org_identifier_system, iam_customer_org_uuid,
                                                     fhir_org_identifier_system, new_tenant_org_uuid,
                                                     parent_org_name+':'+new_tenant_org_name)
                update_fhir_org(org_resource)
            else:
                log_msg = 'FHIR Organization NOT FOUND for the IAM Org : ' + parent_org_name
                logger.warning(log_msg)
            #Update Data Share with qualifier Org
            if isshareupdate:
                update_share_database()

    if not tenant_settings.IS_OPERATION_SUCCESS:
        logger.error('Migration Failed - Retry Again.')
    logger.info('DataServices IAM Customer Data Migration R1.7 to R1.8 - Finished.')
