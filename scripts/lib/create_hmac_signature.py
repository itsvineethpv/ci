﻿#!/usr/bin/python

import hmac
import hashlib
import base64
from datetime import datetime
import sys


def get_api_signing_header(hmac_id, hmac_secret):
    requestVerb = ""
    requestBody = ""
    uriToSign = ""
    queryParamsToSign = ""
    utcDateTime = ""
    proxyname = ""
    utcDateTime = datetime.utcnow().isoformat()
    dotindex = utcDateTime.rfind(".")
    utcDateTime = utcDateTime[0:dotindex] + 'Z'
    saltedHmacSecretSalt = "DHPWS"
    dateHeaderPrepend = "SignedDate:"
    utf8Str = "utf-8"
    authHeaderStr1 = "HmacSHA256;Credential:"
    authHeaderStr2 = ";SignedHeaders:SignedDate"
    authHeaderStr3 = ";Signature:"
    saltedHmacSecret = saltedHmacSecretSalt + hmac_secret
    dateHeader = dateHeaderPrepend + utcDateTime + ";"
    hmacsignature = ""
    finalMessage = saltedHmacSecretSalt + hmac_secret
    # Hashing the current UTC date time in the 1st seed
    firstSeed = utcDateTime.encode(utf8Str)
    finalSignature = (
    hmac.new(finalMessage.encode(utf8Str) , msg=base64.b64encode(firstSeed) , digestmod=hashlib.sha256).digest())
    if requestVerb != "":
        authHeaderStr2 = authHeaderStr2 + ",method"
        # Hashing the verb in the second seed
        secondSeed = (hmac.new(firstSeed , msg=requestVerb.encode(utf8Str) , digestmod=hashlib.sha256).digest())
        firstSeed = secondSeed
    if requestBody != "":
        authHeaderStr2 = authHeaderStr2 + ",body"
        # Hashing the  request payload in the third seed
        thirdSeed = (hmac.new(firstSeed , msg=requestBody.encode(utf8Str) , digestmod=hashlib.sha256).digest())
        firstSeed = thirdSeed
    if queryParamsToSign != "":
        authHeaderStr2 = authHeaderStr2 + ",param"
        # Hashing the query string in the fourth seed
        fourthSeed = (hmac.new(firstSeed , msg=queryParamsToSign.encode(utf8Str) , digestmod=hashlib.sha256).digest())
        firstSeed = fourthSeed
    if uriToSign != "":
        authHeaderStr2 = authHeaderStr2 + ",uri"
        # hash the URI
        sixthSeed = (hmac.new(firstSeed , msg=uriToSign.encode(utf8Str) , digestmod=hashlib.sha256).digest())
        firstSeed = sixthSeed
    finalSignature = (
    hmac.new(finalMessage.encode(utf8Str) , msg=base64.b64encode(firstSeed) , digestmod=hashlib.sha256).digest())
    hmacsignature = base64.b64encode(finalSignature).decode(utf8Str)
    # format: HmacSHA256;Credential:1DrIvmW3VBOyMREkjcAnLEgknDtxmQPyWxnUPKYKdmM=;
    # SignedHeaders:SignedDate;Signature:lvpJhwWLh7//p7DAbFGxmxqJbKbCRPl03/JwC3iJaRA=
    apiSignHeader = authHeaderStr1 + hmac_id + authHeaderStr2 + authHeaderStr3 + hmacsignature

    api_signing_header = {
        'hsdp-api-signature': apiSignHeader,
        'SignedDate': utcDateTime
    }

    return api_signing_header
